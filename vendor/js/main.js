Ezy = {	
	closeFlashMessengerMessage: function(){
		//$('.alert-dismissable.alert-info').hide(10000);
	},
	
	recommendItem: function(event){
		item = $(this);
        $.ajax({
            url : "/admin/ajax/recommend",
            dataType: 'html',
            data: {
                id: $(item).data('itemId')
            },
            success : function(response){
                $(item).replaceWith(response);
            },
            error : function(response){
				$(item).replaceWith(response);
            }
        });
    },
	
	load: function(){
		$('body').on('click', '.ch-recommend-item', Ezy.recommendItem);
	}
}
$(document).ready(Ezy.load);
