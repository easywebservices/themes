<?php 
include_once(realpath(dirname(__FILE__) . '/../../../api/ViewHelperBase.php'));
use \Ezy\ViewHelperBase as ViewHelperBase;

class ViewHelper extends ViewHelperBase {
    
    public function printSpecialOffers($specialOffers, $restaurant) {
        $html = '';
        if( !$specialOffers->getTotal() ) {
         $html .= $this->alert('No special offer currently available.');
         return $html;
         }
        $classes = '';
        if (($specialOffers->getTotal() == 2) || ($specialOffers->getTotal() == 1)) {$classes = 'col-lg-6 col-md-6 col-sm-6 col-xs-12';}
        elseif ($specialOffers->getTotal() == 3) {$classes = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';}
        elseif ($specialOffers->getTotal() == 4) {$classes = 'col-lg-3 col-md-3 col-sm-3 col-xs-12';}
		
        foreach($specialOffers->getItems() as $singleOffer) {
            $html .= "<div class='$classes sng_off_out' itemscope itemtype='http://schema.org/Offer'>";
            $html .= "<div class='sng_off'>";
                $html .= "<p class='ttl'><b><span itemprop='name'>{$singleOffer->getTitle()}</span></b></p>";
                $html .= "<img src='" . $this->view->baseUrl('/tr/themes/ch/theme1/img/fastfood2.png') . "' class='img-responsive' alt='' />";
				
                $html .= "<ul class='list-unstyled'>";
                if ( $singleOffer->getValue() ) {
                    $html .= "<li class='price-bigger'>";
                        if ($restaurant) {
                            $html .= "<span itemprop='priceCurrency' content='{$restaurant->getWebsiteCurrency()}'>{$restaurant->getCurrencySymbol()}</span>";
                        }
                        $html .= "<span itemprop='price'>{$singleOffer->getValue()}</span>";
                    $html .= "</li>";
                }
                
                $html .= "<span itemprop='description'>";
                
                if ( $singleOffer->getDescription() ) {
                    $html .= "<li><small>{$singleOffer->getDescription()}</small></li>";
                }
                if ( $singleOffer->getLine1() ) {
                    $html .= "<li><small>{$singleOffer->getLine1()}</small></li>";
                }
                if ( $singleOffer->getLine2() ) {
                    $html .= "<li><small>{$singleOffer->getLine2()}</small></li>";
                }
                if ( $singleOffer->getLine3() ) {
                    $html .= "<li><small>{$singleOffer->getLine3()}</small></li>";
                }
                if ( $singleOffer->getLine4() ) {
                    $html .= "<li><small>{$singleOffer->getLine4()}</small></li>";
                }
                if ( $singleOffer->getLine5() ) {
                    $html .= "<li><small>{$singleOffer->getLine5()}</small></li>";
                }
                
                $html .= "</span>";
                $html .= "</ul>";
            $html .= "</div>";
            $html .= "</div>";
        }
        return $html;
    }
    
    public function prettyPrintAddress($address) {
        $html = "<h4 class='text-right'>Our Address</h4>";        
        if ( !$address->getCountry() ) {
         $html .= $this->alertWarning('No country information found.');
         return $html;
        }
        $html .= '<div class="text-muted text-right" itemscope itemtype="http://schema.org/PostalAddress">';
        $html .= ($address->getDoorNo() ? "<div itemprop='addressLocality'>{$address->getDoorNo()}</div>" : "");
        $html .= ($address->getHouseName() ? "<div itemprop='addressLocality'>{$address->getHouseName()}</div>" : "");
        $html .= ($address->getRoad() ? "<div itemprop='addressLocality'>{$address->getRoad()}</div>" : "");
        $html .= ($address->getTown() ? "<div itemprop='addressLocality'>{$address->getTown()}</div>" : "");
        $html .= ($address->getCity() ? "<div itemprop='addressLocality'>{$address->getCity()}</div>" : "");
        $html .= ($address->getPostcode() ? "<div itemprop='postalCode'>{$address->getPostcode()}</div>" : "");
        $html .= ($address->getCounty() ? "<div itemprop='addressRegion'>{$address->getCounty()}</div>" : "");
        $html .= ($address->getCountry() ? "<div itemprop='addressCountry'>{$address->getCountry()}</div>" : "");        
        $html .= "</div>";
        return $html;
    }

    public function prettyPrintAddressInOneLine($address) {
        $html  = '<div class="text-muted" itemscope itemtype="http://schema.org/PostalAddress">';
        $html .= ($address->getDoorNo() ? "<span itemprop='addressLocality'>{$address->getDoorNo()}</span>" : "");
        $html .= ($address->getHouseName() ? ", <span itemprop='addressLocality'>, {$address->getHouseName()}</span>" : "");
        $html .= ($address->getRoad() ? ", <span itemprop='addressLocality'>{$address->getRoad()}</span>" : "");
        $html .= ($address->getTown() ? ", <span itemprop='addressLocality'>{$address->getTown()}</span>" : "");
        $html .= ($address->getCity() ? ", <span itemprop='addressLocality'>{$address->getCity()}</span>" : "");
        $html .= ($address->getPostcode() ? ", <span itemprop='postalCode'>{$address->getPostcode()}</span>" : "");
        $html .= ($address->getCounty() ? ", <span itemprop='addressRegion'>{$address->getCounty()}</span>" : "");
        $html .= ($address->getCountry() ? ", <span itemprop='addressCountry'>{$address->getCountry()}</span>" : "");        
        $html .= "</div>";
        return $html;
    }
    
	public function printPageInfo($page){
		if($page->getTitle()){
			$html = "";
			$html .= "<div class='page-info text-center'>";
			$html .= "<h1 class='title'>{$page->getTitle()}</h1>";
			if($page->getDescription()){
				$html .= "<p class='description'>{$page->getDescription()}</p>";
			}
			$html .= "</div>";
			return $html;
		}
	}
}
