<?php 
include_once(realpath(dirname(__FILE__) . '/../../../api/ViewHelperBase.php'));
use \Ezy\ViewHelperBase as ViewHelperBase;

class ViewHelper extends ViewHelperBase {
    
    public function printSpecialOffers($specialOffers, $restaurant = null) {
        $html = '';
        if( !$specialOffers->getTotal() ) {
         $html .= $this->alert('No special offer currently available.');
         return $html;
         }
        $classes = '';
        if (($specialOffers->getTotal() == 2) || ($specialOffers->getTotal() == 1)) {$classes = 'col-lg-6 col-md-6 col-sm-6 col-xs-12';}
        elseif ($specialOffers->getTotal() == 3) {$classes = 'col-lg-4 col-md-4 col-sm-4 col-xs-12';}
        elseif ($specialOffers->getTotal() == 4) {$classes = 'col-lg-3 col-md-3 col-sm-3 col-xs-12';}
        
        foreach($specialOffers->getItems() as $singleOffer) {
            $html .= "<div class='$classes sng_off_out' itemscope itemtype='http://schema.org/Offer'>";
            $html .= "<div class='sng_off'>";
                $html .= "<p class='ttl'><b><span itemprop='name'>{$singleOffer->getTitle()}</span></b></p>";
                $html .= "<img src='" . $this->view->baseUrl('/tr/themes/theme1/img/fastfood2.png') . "' class='img-responsive' alt='' />";
                
                $html .= "<ul class='list-unstyled'>";
                if ( $singleOffer->getValue() ) {
                    $html .= "<li><small>";
                        if ($restaurant) {
                            $html .= "<span itemprop='priceCurrency'>{$restaurant->getWebsiteCurrency()}</span>";
                        }
                        $html .= "<span itemprop='price'>{$singleOffer->getValue()}</span>";
                    $html .= "</small></li>";
                }
                
                $html .= "<span itemprop='description'>";
                
                if ( $singleOffer->getDescription() ) {
                    $html .= "<li><small>{$singleOffer->getDescription()}</small></li>";
                }
                if ( $singleOffer->getLine1() ) {
                    $html .= "<li><small>{$singleOffer->getLine1()}</small></li>";
                }
                if ( $singleOffer->getLine2() ) {
                    $html .= "<li><small>{$singleOffer->getLine2()}</small></li>";
                }
                if ( $singleOffer->getLine3() ) {
                    $html .= "<li><small>{$singleOffer->getLine3()}</small></li>";
                }
                if ( $singleOffer->getLine4() ) {
                    $html .= "<li><small>{$singleOffer->getLine4()}</small></li>";
                }
                if ( $singleOffer->getLine5() ) {
                    $html .= "<li><small>{$singleOffer->getLine5()}</small></li>";
                }
                
                $html .= "</span>";
                $html .= "</ul>";
            $html .= "</div>";
            $html .= "</div>";
        }
        return $html;
    }
    
    public function prettyPrintAddress($address) {
        $html = "<h4>Our Address</h4>";        
        if ( !$address->getCountry() ) {
         $html .= $this->alertWarning('No country information found.');
         return $html;
        }
        $html .= '<div itemscope itemtype="http://schema.org/PostalAddress">';
        $html .= ($address->getDoorNo() ? "<div itemprop='addressLocality'>{$address->getDoorNo()}</div>" : "");
        $html .= ($address->getHouseName() ? "<div itemprop='addressLocality'>{$address->getHouseName()}</div>" : "");
        $html .= ($address->getRoad() ? "<div itemprop='addressLocality'>{$address->getRoad()}</div>" : "");
        $html .= ($address->getTown() ? "<div itemprop='addressLocality'>{$address->getTown()}</div>" : "");
        $html .= ($address->getCity() ? "<div itemprop='addressLocality'>{$address->getCity()}</div>" : "");
        $html .= ($address->getPostcode() ? "<div itemprop='postalCode'>{$address->getPostcode()}</div>" : "");
        $html .= ($address->getCounty() ? "<div itemprop='addressRegion'>{$address->getCounty()}</div>" : "");
        $html .= ($address->getCountry() ? "<div itemprop='addressCountry'>{$address->getCountry()}</div>" : "");        
        $html .= "</div>";
        return $html;
    }

    public function prettyPrintAddressInOneLine($address) {
        $html  = '';
        $html .= ($address->getDoorNo() ? "<h3 class='margin-0 line-height-35'>{$address->getDoorNo()}" : "");
        $html .= ($address->getHouseName() ? " - {$address->getHouseName()}" : " - ");
        $html .= ($address->getRoad() ? "{$address->getRoad()}" : "");
        $html .= ($address->getTown() ? "<br />{$address->getTown()}</h3>" : "");
        $html .= ($address->getCity() ? "<h3 class='margin-0 line-height-35'>{$address->getCity()}" : "<h3 class='margin-0 line-height-35'>");
        $html .= ($address->getPostcode() ? "{$address->getPostcode()}" : "");
        $html .= ($address->getCounty() ? "<br />{$address->getCounty()}" : "");
        $html .= ($address->getCountry() ? ", {$address->getCountry()}</h3>" : "");        
        return $html;
    }
    
}
