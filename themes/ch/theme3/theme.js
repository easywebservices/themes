$(document).ready(function() {
   $('#nav').slicknav();
   $(".fancybox").fancybox();
   function fixDiv() {
    var $div = $("#navwrap");
		if ($(window).scrollTop() > $div.data("top")) { 
			$('#navwrap').css({'position': 'fixed', 'top': '0', 'z-index': '999', 'width': '100%'}); 
		}
		else {
			$('#navwrap').css({'position': 'static', 'top': 'auto', 'width': '100%'});
		}
	}

$("#navwrap").data("top", $("#navwrap").offset().top); // set original position on load
$(window).scroll(fixDiv);

   $('.increase').click (function() {
      $('.section').addClass('padding-top-90');
   });

});