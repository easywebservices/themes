<?php
   require "lessc.inc.php";
   $less = new lessc;
   echo $less->checkedCompile("cadet-blue.less", "../cadet-blue.css");
   echo $less->checkedCompile("dark-khaki.less", "../dark-khaki.css");
   echo $less->checkedCompile("dark-slate-blue.less", "../dark-slate-blue.css");
   echo $less->checkedCompile("slate-gray.less", "../slate-gray.css");
?>