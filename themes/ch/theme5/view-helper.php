<?php 
include_once(realpath(dirname(__FILE__) . '/../../../api/ViewHelperBase.php'));
use \Ezy\ViewHelperBase as ViewHelperBase;

class ViewHelper extends ViewHelperBase {
    
    public function printSpecialOffers($specialOffers, $restaurant = null, $visibility) {
        $html = '';
        if( !$specialOffers->getTotal() ) {
         $html .= $this->alert('No special offer currently available.');
         return $html;
         }
        $classes = '';
        if (($specialOffers->getTotal() == 2) || ($specialOffers->getTotal() == 1)) {$classes = 'col-lg-6 col-md-6 col-sm-6 margin-15-auto-0 col-xs-12';}
        elseif ($specialOffers->getTotal() == 3) {$classes = 'col-lg-4 col-md-4 margin-15-auto-0 col-sm-4 col-xs-12';}
        elseif ($specialOffers->getTotal() == 4) {$classes = 'col-lg-3 col-md-3 margin-15-auto-0 col-sm-3 col-xs-12';}
        
        foreach($specialOffers->getItems() as $singleOffer) {
            $html .= "<div class='' itemscope itemtype='http://schema.org/Offer'>";
            $html .= "<div class='$classes text-center single-featured'>";
            $html .= "<img src='" . $this->view->baseUrl('/tr/themes/ch/theme5/img/offer.jpg') . "' class='' alt='' />";
            $html .= "<p class='title' itemprop='name'>{$singleOffer->getTitle()}</p>";

				 $html .= "<ul class='list-unstyled'>";
                if ( $singleOffer->getValue() ) {
                    $html .= "<li class='price'>";
                        if ($restaurant) {
                            $html .= "<span itemprop='priceCurrency' content='{$restaurant->getWebsiteCurrency()}'>{$restaurant->getCurrencySymbol()}</span>";
                        }
                        $html .= "<span itemprop='price'>{$singleOffer->getValue()}</span>";
                    $html .= "</li>";
                }
                
                $html .= "<span itemprop='description'>";
                
                if ( $singleOffer->getDescription() ) {
                    $html .= "<li><small>{$singleOffer->getDescription()}</small></li>";
                }
                if ( $singleOffer->getLine1() ) {
                    $html .= "<li>{$singleOffer->getLine1()}</li>";
                }
                if ( $singleOffer->getLine2() ) {
                    $html .= "<li>{$singleOffer->getLine2()}</li>";
                }
                if ( $singleOffer->getLine3() ) {
                    $html .= "<li>{$singleOffer->getLine3()}</li>";
                }
                if ( $singleOffer->getLine4() ) {
                    $html .= "<li>{$singleOffer->getLine4()}</li>";
                }
                if ( $singleOffer->getLine5() ) {
                    $html .= "<li>{$singleOffer->getLine5()}</li>";
                }
				 
				 $html .= "</span>";
				 $html .= "</ul>";
               if ( $visibility == 'yes' ) {
						$html .= "<a class='readmore' href='#'>see details</a>";
					}
			$html .= "</div>";
			$html .= "</div>";
        }
        return $html;
    }
    
    public function prettyPrintAddress($address) {
        $html = "<h4>Our Address</h4>";        
        if ( !$address->getCountry() ) {
         $html .= $this->alertWarning('No country information found.');
         return $html;
        }
        $html .= '<div itemscope itemtype="http://schema.org/PostalAddress">';
        $html .= ($address->getDoorNo() ? "<div itemprop='addressLocality'>{$address->getDoorNo()}</div>" : "");
        $html .= ($address->getHouseName() ? "<div itemprop='addressLocality'>{$address->getHouseName()}</div>" : "");
        $html .= ($address->getRoad() ? "<div itemprop='addressLocality'>{$address->getRoad()}</div>" : "");
        $html .= ($address->getTown() ? "<div itemprop='addressLocality'>{$address->getTown()}</div>" : "");
        $html .= ($address->getCity() ? "<div itemprop='addressLocality'>{$address->getCity()}</div>" : "");
        $html .= ($address->getPostcode() ? "<div itemprop='postalCode'>{$address->getPostcode()}</div>" : "");
        $html .= ($address->getCounty() ? "<div itemprop='addressRegion'>{$address->getCounty()}</div>" : "");
        $html .= ($address->getCountry() ? "<div itemprop='addressCountry'>{$address->getCountry()}</div>" : "");        
        $html .= "</div>";
        return $html;
    }

    public function prettyPrintAddressInOneLine($address) {
        $html  = '';
        $html .= ($address->getDoorNo() ? "<h3 class='margin-0 line-height-35'>{$address->getDoorNo()}" : "");
        $html .= ($address->getHouseName() ? " - {$address->getHouseName()}" : " - ");
        $html .= ($address->getRoad() ? "{$address->getRoad()}" : "");
        $html .= ($address->getTown() ? "<br />{$address->getTown()}</h3>" : "");
        $html .= ($address->getCity() ? "<h3 class='margin-0 line-height-35'>{$address->getCity()}" : "<h3 class='margin-0 line-height-35'>");
        $html .= ($address->getPostcode() ? "{$address->getPostcode()}" : "");
        $html .= ($address->getCounty() ? "<br />{$address->getCounty()}" : "");
        $html .= ($address->getCountry() ? ", {$address->getCountry()}</h3>" : "");        
        return $html;
    }
    
   public function limitWords($string, $word_limit) {
      $words = explode(' ', $string);
      return implode(' ', array_slice($words, 0, $word_limit)); 
   }
    
}
