<?php
namespace Ezy;

include_once('Base.php');
include_once('MenuCategory.php');
include_once('MenuCategoryClassic.php');
include_once('MenuCategoryNormal.php');

use \Ezy\Base as Base;
use \Ezy\MenuCategory as MenuCategory;
use \Ezy\MenuCategoryClassic as MenuCategoryClassic;
use \Ezy\MenuCategoryNormal as MenuCategoryNormal;

class Menu extends Base {
    public $classic_categories;
    public $normal_categories;
    
    public function __construct($options = null){
        parent::__construct($options);
        
        if (! $this->normal_categories instanceof Feed && is_array($this->normal_categories)) { 
            $this->normal_categories = new Feed($this->normal_categories);
        }
        
        if (! $this->classic_categories instanceof Feed && is_array($this->classic_categories)) { 
            $this->classic_categories = new Feed($this->classic_categories);
        }
    }
    
    public function getClassicCategories(){
		if ($this->classic_categories instanceof Feed && is_array($this->classic_categories->getItems())) {
            $items = $this->classic_categories->getItems();
            $lastCat = end($items);
            
            if (!$lastCat instanceof \Ezy\MenuCategoryClassic) {
                $tmpCats = array();
                foreach ($this->classic_categories->getItems() as $cat) {
                    $tmpCats[] = new Ezy\MenuCategoryClassic($cat);
                }
                
                $this->classic_categories = $tmpCats;
            }
        }
        if (!$this->classic_categories instanceof Feed) {
			$this->classic_categories = new Feed();
		}
		
        return $this->classic_categories;
    } 
    
    public function getNormalCategories(){
		if ($this->normal_categories instanceof Feed && is_array($this->normal_categories->getItems())) {
            $items = $this->normal_categories->getItems();
            $lastCat = end($items);
            
            if (!$lastCat instanceof \Ezy\MenuCategoryNormal) {
                $tmpCats = array();
                foreach ($this->normal_categories->getItems() as $cat) {
                    $tmpCats[] = new Ezy\MenuCategoryNormal($cat);
                }
                
                $this->normal_categories = $tmpCats;
            }
        }
        if (!$this->normal_categories instanceof Feed) {
			$this->normal_categories = new Feed();
		}
        return $this->normal_categories;
    }
    
    
    public function getCategories($menuCategoryType = null){
        
        if ($menuCategoryType == null) {
            $normal = $this->getNormalCategories();
            $classic = $this->getClassicCategories();
            
            $feed = new Feed(array('type' => 'MenuCategory', 'items' => array()));
			$items = array_merge($normal->getItems(), $classic->getItems());
            $feed->setItems($items)->setTotal(count($items));
            return $feed;
        }
        
        if ($menuCategoryType == self::MENU_CATEGORY_TYPE_CLASSIC) {
            return $this->getClassicCategories();
        }
        
        if ($menuCategoryType == self::MENU_CATEGORY_TYPE_NORMAL) {
            return $this->getNormalCategories();
        }
    } 
}
