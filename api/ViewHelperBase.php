<?php
namespace Ezy;
include_once('RecommendButton.php');
include_once('ReviewForm.php');
include_once('SpiceLevel.php');
include_once('Pagination.php');

abstract class ViewHelperBase {
    public $view;
    
    public function __construct() {        
        $this->view = new FakeViewCLass();
    }
    
    public function pagination($feed) {
        return new \Ezy\Pagination($feed);
    }
    
    public function recommendButton($user, $restaurant = null) {
        return new \Ezy\RecommendButton($user, $restaurant);        
    }
    
    public function reviewForm() {
        return new \Ezy\ReviewForm();        
    }
    
    public function alert ($text, $classes = 'alert alert-warning') {
      return "<div class='text-center $classes'><br />$text <div class='clearfix'></div> <br /></div>";
    }
    
    public function alertWarning ( $text ) {
      return $this->alert( $text, 'alert alert-warning' );
    }
    
    public function alertInfo ( $text ) {
      return $this->alert ( $text, 'alert alert-info' );
    }
    
    public function alertDanger ( $text ) {
      return $this->alert ( $text, 'alert alert-danger' );
    }
    
    public function alertSuccess ( $text ) {
      return $this->alert ( $text, 'alert alert-success' );
    }
    
    public function spiceLevel($restaurant) {
        return new \Ezy\SpiceLevel($restaurant);       
    }
    
    public function getSeoMetaTags($seo) {
        // depricated
        return '';
    }
    
    public function getGaAnalyticsScript($seo) {
        //depricated
        return '';
    }
}

class FakeViewClass {
    public $base_url;   
    
    public function baseUrl($link) {
        if (! defined('PUBLIC_PATH')) {
            $link = str_replace('/tr/', "/" . basename(dirname(__DIR__)) ."/", $link);
        }
        
        return $this->base_url . $link;
    }
}
