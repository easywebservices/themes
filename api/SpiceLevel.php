<?php 
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class SpiceLevel extends Base {
    
    private $_restaurant ;
    
    public function __construct($restaurant) {
        $this->_restaurant = $restaurant;     
        return $this;
    }

    public function printImage($foodItem) {
        if ($this->_restaurant->getWebsiteShowSpiceLevel() && $foodItem->getSpiceLevel() > 0 ) {
            $level = $foodItem->getSpiceLevel();
            $levelName = \Ezy\Ezy::getSpiceLevelName($level);
            $theme = $this->_restaurant->getTheme();
            
            if (is_file($theme->themeBaseUrl() . "img/spice-level-{$level}.png")) {
                $imgUrl = $theme->themeBaseUrl() . "img/spice-level-{$level}.png";
            } else {
                $imgUrl = "/" . basename(dirname(__DIR__)) . "/img/spice-level-{$level}.png";
            }
            
            return "<img src='{$imgUrl}' alt='' title='$levelName'/>";
        }
    }
}