<?php
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

abstract class MenuItem extends Base{
    
    public $id;
    public $category_id;
    public $name;    
    public $description;
    public $price; // for Classic items price is chicken price
    public $spice_level;
    public $recommended;
    public $recommended_by_me;
    
    public function __construct($options = null){
        parent::__construct($options);
    } 
    
    public function setId($value){
        $this->id = $value;
        return $this;
    }
    public function getId(){
        return $this->id;
    }   
    
    public function setCategoryId($value){
        $this->category_id = $value;
        return $this;
    }
    public function getCategoryId(){
        return $this->category_id;
    }    
    
    public function setName($value){
        $this->name = $value;
        return $this;
    }
    public function getName(){
        return $this->escape($this->name);
    }    
    
    public function setDescription($value){
        $this->description = $value;
        return $this;
    }
    public function getDescription(){
        return $this->escape($this->description);
    }        
    
    public function setPrice($value){
        $this->price = $value;
        return $this;
    }
    public function getPrice(){
        return $this->escape($this->price);
    }
    
    public function setSpiceLevel($value){
        $this->spice_level = $value;
        return $this;
    }
    public function getSpiceLevel(){
        return $this->spice_level;
    }
    
    public function setRecommended($value){
        $this->recommended = $value;
        return $this;
    }
    public function getRecommended(){
        return $this->recommended;
    }
    
    public function setRecommendedByMe($value){
        $this->recommended_by_me = $value;
        return $this;
    }
    public function getRecommendedByMe(){
        return $this->recommended_by_me;
    }
    public function recommendedByThisUser(){
        $user = Zend_Registry::get(\Ezy\User::CURRENT_USER);
        
        if ($user->isLoggedIn()) {
            return $this->recommended_by_me;
        }
    }
}