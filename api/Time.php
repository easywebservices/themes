<?php
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class Time extends Base{

    public $days;
    public $lunch_start;
    public $lunch_end;
    public $diner_start;
    public $diner_end;
    public $delivery_start;
    public $delivery_end;

    public function __construct($options = null){
        parent::__construct($options);
    }

    public function setDays($value){
        $this->days = $value;
        return $this;
    }
    public function getDays(){
        return $this->escape($this->days);
    }

    public function setLunchStart($value){
        $this->lunch_start = $value;
        return $this;
    }
    public function getLunchStart(){
        return $this->escape($this->lunch_start);
    }

    public function setLunchEnd($value){
        $this->lunch_end = $value;
        return $this;
    }
    public function getLunchEnd(){
        return $this->escape($this->lunch_end);
    }

    public function setDinerStart($value){
        $this->diner_start = $value;
        return $this;
    }
    public function getDinerStart(){
        return $this->escape($this->diner_start);
    }

    public function setDinerEnd($value){
        $this->diner_end = $value;
        return $this;
    }
    public function getDinerEnd(){
        return $this->escape($this->diner_end);
    }

    public function setDeliveryStart($value){
        $this->delivery_start = $value;
        return $this;
    }
    public function getDeliveryStart(){
        return $this->escape($this->delivery_start);
    }

    public function setDeliveryEnd($value){
        $this->delivery_end = $value;
        return $this;
    }
    public function getDeliveryEnd(){
        return $this->escape($this->delivery_end);
    }
}