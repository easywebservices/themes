<?php
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class Theme extends Base {
    public $ref;
    public $name;
    public $developer_name;
    public $developer_repo;
    public $price;
    public $features;
    public $currency;
    public $status;    
    public $version_no;
    public $available_color_schemes = array();
    
    public $album;
    public $screenshots;
    public $helpers;    
        
    const VIEW_HELPER_FILE_NAME = "view-helper.php";
    const VIEW_HELPER_CLASS_NAME = "ViewHelper";
    
    public function __construct($options = null){
        parent::__construct($options);
    }
    
    public function isValid(){
        $errors = array();
        if (! is_dir($this->themeDirectoryLocation())) {
            if (getenv('APPLICATION_ENV') != 'production') {
                $errors[] = $this->themeDirectoryLocation() . " is not a valid directory location.";                
            }
        }
        
        /* Disabling this check for development
        
        if (! $this->isPublished()) {
            if (getenv('APPLICATION_ENV') != 'production') {                
                $errors[] = "Theme is not published.";
            }
        }
        
        */
        
        if (! is_dir($this->themeDirectoryLocation() . self::THEMES_SCREENSHOT_DIR_NAME)) {
            if (getenv('APPLICATION_ENV') != 'production') {
                $errors[] = "No directory for screenshots found!";
            }
        }
        
        if (! is_file($this->getLayoutFileUrl())) {
            if (getenv('APPLICATION_ENV') != 'production') {
                $errors[] = $this->getLayoutFileUrl() . " is not a valid layout file.";                
            }
        }
        
        $availableColorSchemes = $this->getAvailableColorSchemes();
        if (!empty($availableColorSchemes)) {
            $cssFiles = $this->getCssSources($fileNamesOnly = true);
            
            foreach ($availableColorSchemes as $colorSchemeName => $v) {  
                if (!isset($cssFiles[ $colorSchemeName . ".css" ]) && !isset($cssFiles[ $colorSchemeName . ".CSS" ])) {
                    $errors[] = "No CSS file found for color scheme '$colorSchemeName'";
                }
            }
        }
        
        if (!empty($errors)) {
            throw new \Ezy\EzyException(implode(" *** ", $errors));
        }
        
        return true;
    }    
        
    public function getColorSchemeCssFile($value){
        return $this->themeBaseUrl() .'color_bank/'.  strtolower($value)  . ".css";
    }
    
    public function getAvailableColorSchemes($asString = false){        
        if($asString){
            return implode(', ', $this->available_color_schemes);
        }
        
        return $this->available_color_schemes;
    }
    
    public function getScreenshots() {
        if (!$this->screenshots instanceof \Ezy\Album) {
            $this->screenshots = $this->readAlbumDirectory(self::THEMES_SCREENSHOT_DIR_NAME);
        }
        
        return $this->screenshots;        
    }
    
    public function getAlbum($type) {
        if (!$this->album instanceof \Ezy\Album) {
            $this->album = $this->readAlbumDirectory(self::DEFAULT_THEME_ALBUM_DIR, $type);
        }
        
        return $this->album;        
    }
    
    public function readAlbumDirectory($albumDir, $subDir = null) {        
		$loc = $this->themeDirectoryLocation() . $albumDir . ($subDir ? "/$subDir/" : "/");
        $files = $this->readDirectory($loc);
        
        $album = new \Ezy\Album();
        $photos = array();
        $int = 1;
        foreach($files as $l){
            $tfb = explode('.', $l);
            $tmp = end($tfb);
            if( $tmp == 'jpg' || $tmp == 'JPG' || 
                $tmp == 'jpeg' || $tmp == 'JPEG' || 
                $tmp == 'png' || $tmp == 'PNG' || 
                $tmp == 'gif' || $tmp == 'GIF'){
                
                $photos[$l] = new \Ezy\Photo();
                $fileLocation = $this->themeBaseUrl() . $albumDir . ($subDir ? "/$subDir/" : "/") . $l;
                $photos[$l]->setImageUrl($fileLocation);
                
                if ($int == 1) {
                    $album->setThumbnailUrl($fileLocation);
                    $int++;
                }
            }
        }
            
        if (!empty($photos)) {
            $photosFeed = new \Ezy\Feed(array('items' => $photos, 'rows' => null, 'total' => count($photos)));
            $album->setPhotos($photosFeed);
        }
        
        return $album;
    }
    
    public function themeBaseUrl() {
        return "/" . basename(dirname(__DIR__)) . "/themes/{$this->getDeveloperRepo()}/{$this->getId()}/";
    }
    
    public function getThumbnailUrl(){
        if ($this->getScreenshots() instanceof \Ezy\Album) {
            return $this->getScreenshots()->getThumbnailUrl();
        }
    }

    public function readDirectory( $dir ){
        if ( !is_dir($dir) ) {
            throw new \RuntimeException("There is no such directory '$dir'");
        }

        return scandir($dir);
    }
    
    public function getJsSources(){
        $loc = $this->themeDirectoryLocation();
        $files = $this->readDirectory($loc);
        $newSrcArray = array();
        
        foreach($files as $l){
        	$tmp = end(explode('.', $l));
        	if($tmp == 'js' || $tmp == 'JS'){
            	$newSrcArray[$l] = $loc . $l;
        	}
        }
        
        return $newSrcArray;
    }
    
    public function getCssSources($fileNamesOnly = false){
        $loc = $this->themeDirectoryLocation() . "color_bank/";
        $files = $this->readDirectory($loc);
        $newSrcArray = array();
        
        foreach($files as $l){
            $all = explode('.', $l);
        	$tmp = end($all);
        	if($tmp == 'css' || $tmp == 'CSS'){
                if ($fileNamesOnly) {
                    $newSrcArray[$l] = $l;
                } else {
                    $newSrcArray[$l] = $loc . $l;
                }
        	}
        }
        
        return $newSrcArray;
    }

    public function getFileSources(){
        $loc = $this->themeDirectoryLocation();
        $files = $this->readDirectory($loc);
        $newSrcArray = array();
        
        foreach($files as $l){
        	$tmp = end(explode('.', $l));
        	if($tmp == 'html' || $tmp == 'HTML' || $tmp == 'phtml' || $tmp == 'PHTML'){
            	$newSrcArray[$l] = $loc . $l;
        	}
        }
        
        return $newSrcArray;
    }

    public function getImageSources(){
        $loc = $this->themeDirectoryLocation();
        $files = $this->readDirectory($loc);
        $newSrcArray = array();
        
        foreach($files as $l){
        	$tmp = end(explode('.', $l));
        	if($tmp == 'jpg' || $tmp == 'jpeg' || $tmp == 'JPG' || $tmp == 'JPEG' || $tmp == 'png' || $tmp == 'PNG'){
            	$newSrcArray[$l] = $loc . $l;
        	}
        }
        
        return $newSrcArray;
    }
    

    public function themeDirectoryLocation(){
        return realpath(dirname(__FILE__)) . "/../themes/{$this->getDeveloperRepo()}/{$this->getRef()}/";
    }
    
    public function getLayoutFileUrl(){
        $loc = $this->themeDirectoryLocation() . self::THEMES_LAYOUT_FILE_NAME;        
        if(file_exists($loc)){
            return $loc;
        }
    }
    
    public function isPublished(){       
        $globalConfigs = include(dirname(__FILE__) . "/../configs.php");
        if (!is_array($globalConfigs)) {
            throw new \Ezy\EzyException("Global Configuration file load failed.");
        }
        
        if (isset($globalConfigs[ $this->getRef() ])) {
            return ($globalConfigs[ $this->getRef() ]['status'] == 'published');
        }
        
    }

    public function getJsFileUrl(){
        $loc = $this->themeDirectoryLocation() . self::DEFAULT_FILE_NAME . ".js";
        if(!file_exists($loc)){
            throw new Exception("No JS file found at ($loc).");
        }

        return $loc;
    }
    
    public function setRef($value){
        $this->ref = $value;
        return $this;
    }
    public function getRef(){
        return $this->ref;
    }
    public function getId(){
        return $this->getRef();
    }

    public function setName($value){
        $this->name = $value;
        return $this;
    }
    public function getName(){
        return $this->escape($this->name);
    }    

    public function setDeveloperName($value){
        $this->developer_name = $value;
        return $this;
    }
    public function getDeveloperName(){
        return $this->escape($this->developer_name);
    }
    public function getDeveloper(){
        return $this->getDeveloperName();
    }
   
    public function setDeveloperRepo($value){
        $this->developer_repo = $value;
        return $this;
    }
    public function getDeveloperRepo(){
        return $this->escape($this->developer_repo);
    }
    
    public function setPrice($value){
        $this->price = $value;
        return $this;
    }
    public function getPrice(){
        return $this->escape($this->price);
    }
    
    public function setFeatures($value){
        $this->features = $value;
        return $this;
    }
    public function getFeatures(){
        return $this->features;
    }
    
    public function setCurrency($value){
        $this->currency = $value;
        return $this;
    }
    public function getCurrency(){
        return $this->escape($this->currency);
    }
    
    public function setStatus($value){
        $this->status = $value;
        return $this;
    }
    public function getStatus(){
        return $this->escape($this->status);
    }
    
    public function getHelpers() {
        $fileName = $this->themeDirectoryLocation() . self::VIEW_HELPER_FILE_NAME;
        if (! is_file($fileName)) {
            throw new Exception("ViewHelper file not found for theme {$this->getName()}. Please create a class file called " . self::VIEW_HELPER_FILE_NAME . "} consisting a public class ViewHelper on it that consists your view helper public functions. Use these functions in a view like this '\$view->viewHelper()->addressPrittyPrint()'");
        }
        
        include_once($fileName);
        
        if (! class_exists(self::VIEW_HELPER_CLASS_NAME)) {
            throw new Exception("ViewHelper file not found for theme {$this->getName()}. Please create a class file called " . self::VIEW_HELPER_FILE_NAME . "} consisting a public class ViewHelper on it that consists your view helper public functions. Use these functions in a view like this '\$view->viewHelper()->addressPrittyPrint()'");
        }
        
        $className = self::VIEW_HELPER_CLASS_NAME;
        $this->helpers = new $className();
        return $this->helpers;
    }


    public function setVersionNo($value){
        $this->version_no = $value;
        return $this;
    }
    public function getVersionNo(){
        return $this->escape($this->version_no);
    }
}
