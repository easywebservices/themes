<?php
namespace Ezy;

class Ezy extends Base {

    const SESS_EZY = 'X_Ezy';
    
    /*
    *   ONLY valid for the API
    */
    public static function findTheme($ref) {
        $globalConfigs = include(dirname(__FILE__) . "/../configs.php");
        if (!is_array($globalConfigs)) {
            throw new \Ezy\EzyException("Global Configuration file load failed.");
        }
        
        return (isset($globalConfigs[ $ref ]) ? new \Ezy\Theme($globalConfigs[ $ref ])
            : null);
    }
    
    public static function getTestRestaurant() {
        @session_start();
        $_SESSION[ self::SESS_EZY ] = null;
        
        $dataFileName = (isset($_GET['data']) ? $_GET['data'] : "data");
        $data = require_once(realpath( dirname(__FILE__) ) . "/$dataFileName.php");
        $_SESSION[ self::SESS_EZY ] = new \Ezy\Restaurant($data);
        
        return $_SESSION[ self::SESS_EZY ];
    }
    
    public static function getRatingOptions() {
        return array(
            1 => "1 Star - I didn't like", 
            2 => "2 Star - Can do well", 
            3 => "3 Star - OK", 
            4 => "4 Star - Liked it!", 
            5 => "5 Star - Loved it!", 
        );
    }
    
    public static function getPageNames() {
        return array(            
            'home', 'about', 'menu', 'videos', 'photos', 'reviews', 'offers'
        );
    }    
    
    public static function getStatuses(){
        // can't change the sequence
        return array(
            0 => self::STATUS_QUEUED,
            1 => self::STATUS_PUBLISHED,
            2 => self::STATUS_RETIRED,
            3 => self::STATUS_REPORTED,
        );
    }
    
    public static function getStatusKeyByValue( $value ){
        $statuses = self::getStatuses();        
        return array_search($value, $statuses);
    }
    
    public static function getStatusByKey( $key ){
        $statuses = self::getStatuses();
        return (isset($statuses[ $key ]) ? $statuses[ $key ] : trigger_error("Undefined status key $key"));
    }
    
    public static function getStatusNameByValue( $value ){
        if($value == self::STATUS_QUEUED) return "Queued";
        if($value == self::STATUS_PUBLISHED) return "Published";
        if($value == self::STATUS_RETIRED) return "Retired";
        if($value == self::STATUS_REPORTED) return "Reported";
    }
    
    public static function getStatusNameByKey( $key ){
        $value = self::getStatusByKey( $key );
        if($value == self::STATUS_QUEUED) return "Queued";
        if($value == self::STATUS_PUBLISHED) return "Published";
        if($value == self::STATUS_RETIRED) return "Retired";
        if($value == self::STATUS_REPORTED) return "Reported";
    }
    
    public static function getAccessLevels(){
        // can't change the sequence
        return array(
            5 => User_Model_User::ACCESS_LEVEL_ADMIN,
            4 => User_Model_User::ACCESS_LEVEL_STAFF,
            3 => User_Model_User::ACCESS_LEVEL_DEVEL,
            2 => User_Model_User::ACCESS_LEVEL_OWNER,
            1 => User_Model_User::ACCESS_LEVEL_CUSTOMER,
            0 => User_Model_User::ACCESS_LEVEL_GUEST,
        );
    }
    
    public static function getAccesslevelKeyByValue( $value ){
        $als = User_Model_User::getAccessLevels();
        return array_search($value, $als);
    }
    
    public static function getAccessLevelByKey( $key ){
        $als = User_Model_User::getAccessLevels();        
        return $als[$key ];
    }
    
    public static function getUserTypes(){
        return array(
            User_Model_User::ACCESS_LEVEL_OWNER => "Owner",
            User_Model_User::ACCESS_LEVEL_CUSTOMER => "Customer",
            User_Model_User::ACCESS_LEVEL_GUEST    => "Guest",
        );
    }  
    
    public static function getSpiceLevels(){
        return array(
            '1' => 'Very mild', '2' => 'Mild', '3' => 'Spicy', '4' => 'Medium', '5' => 'Hot', '6' => 'Extremely Hot'
        );
    }   
    
    public static function getSpiceLevelName($level){
        $sls = self::getSpiceLevels();        
        if(isset($sls[$level])){
            return $sls[$level];
        }
    }    
    
    public static function getCurrencySymbols(){
        return array(
            'GBP' => '&pound;',
            //'USD' => 'USD (&dollar;)',
            //'EU' => 'EURO (&euro;)',
        );
    }   
    
    public static function getOnlyAllowedCountryIsoPair(){
        $conf = Zend_Registry::get('config');
        return $conf->getCountryList();
    }
}
