﻿<?php 
return array(
  "id"=> "55bdb84ef6b1a85df8f4192fa5d94526",
  "owner_id"=>"60117877d59a98a9443aff06c40a6685",
  "developer_id"=> NULL,
  "admin_id"=> "999b5fa3d2e333f193c768567d8898f5",
  "default_album_id"=> null,
  "type"=> "chinese",
  "name"=> "Cinnamon",
  "slogan"=> null,
  "logo"=> NULL,
  "description"=> null,
  "package"=> NULL,
  "no_of_seats"=> NULL,
  "phone_reservation"=> NULL,
  "phone_takeaway"=> NULL,
  "delivery_radious"=> NULL,
  "delivery_radious_type"=> NULL,
  "door_no"=> NULL, 
  "house_name"=> NULL,
  "road"=> NULL,
  "town"=> NULL,
  "city"=> NULL,
  "postcode"=> NULL,
  "county"=> NULL,
  "country"=> NULL,
  "social_facebook_page_url"=> NULL,
  "social_google_plus_page_url"=> NULL,
  "seo_varification_google"=> NULL,
  "seo_varification_bing"=> NULL,
  "seo_ga_account_id"=> NULL,
  "seo_description"=> NULL,
  "seo_keywords"=> NULL,
  "theme_ref"=> NULL,
  "theme_color_scheme"=> NULL,
  "default_image_width"=> NULL,
  "default_video_id"=> NULL,
  "website_currency"=> NULL,
  "website_language"=> NULL,
  "website_url"=> NULL,
  "website_availabile"=>  NULL,
  "website_show_spice_level"=> NULL,
  "website_show_recommend_button"=> NULL,
  "website_footer_link"=> NULL,
  "website_footer_link_text"=> NULL,
  "website_powered_by_text"=> NULL,
  "show_page_offers"=> false,
  "show_page_about"=>false,
  "show_page_reviews"=>false,
  "show_page_videos"=>false,
  "show_page_photos"=> false,
  "show_page_reservation"=> false,  
  "accept_cheque"=> NULL,
  "accept_cash"=> NULL,
  "accept_card_visa"=> NULL,
  "accept_card_master"=> NULL,
  "accept_card_maestro"=> NULL,
  "accept_card_american_express"=> NULL,
  "available_eatin"=>NULL,
  "available_takeaway"=>NULL,
  "available_delivery"=> NULL,
  "status" => NULL,
  "ts"=> NULL,
  
  "albums"=> array(
    "type"=> 'Album',
    "start"=> 0,
    "rows"=> 20,
    "total"=> 0,
    "items"=> array( ),
  ),
  "offers"=> array(
        "type"=>"Offer",
        "start"=> 0,
        "rows"=> 20,
        "total"=> 0,
        "items"=> array()
    ),
  
  "reviews"=> array(  
    "type"=> 'Review',
    "start"=> 0,
    "rows"=> 20,
    "total"=> 0,
        "items"=> array()
    ),
  
  "times"=> array(
    "type"=>      'Time',
    "start"=> 0,
    "rows"=> 5,
    "total"=> 0,
        "items"=> array()
    ),
  
  "customers"=>  array(
    "type"=>    "Customer",
    "start"=> 0,
    "rows"=> 20,
    "total"=> 0,
    "items"=> array(),    
  ),
  
  "videos"=> array(
    "type"=>      'Video',
    "start"=> 0,
    "rows"=> 20,
    "total"=> 0,
        "items"=> array()
    ),
   
  "pages"=> array(
    "type"=>  'Page',
    "start"=> 0,
    "rows"=> 20,
    "total"=> 0,
        "items"=> array()
    ),
    
  "menu"=> array(
    "normal_categories" => array(
        "type"=> 'MenuCategoryNormal',
        "start"=> 0,
        "rows"=> 20,
        "total"=> 0,
        "items"=> array()
    ),
    "classic_categories" => array (
        "type"=> 'MenuCategoryClassic',
        "start"=> 0,
        "rows"=> 20,
        "total"=> 0,
        "items"=> array()
    ),
  ),
);