<?php 
namespace Ezy;

class ReviewForm {

    public  $form_classes = '';
    public  $select_classes = 'form-control';
    public  $textarea_classes = 'form-control';
    public  $submit_classes = 'btn btn-primary';
    
    private $_html = '';
    
    public function getHtml() {
        $this->_html  = $this->_getFormElementStart();
        $this->_html .= "<div class='clearfix'></div>". $this->_getRatingElement();
        $this->_html .= "<br/>";
        $this->_html .= "<div class='clearfix'></div>". $this->_getTextElement();
        $this->_html .= "<br/>";
        $this->_html .= $this->_getFormElementEnd();
        return $this->_html;
    }
    
    
    public function setFormClasses($value) {
        $this->form_classes = $value;
        return $this;
    }    
    public function getFormClasses() {
        return $this->form_classes;
    }

    
    public function setSubmitClasses($value) {
        $this->submit_classes = $value;
        return $this;
    }    
    public function getSubmitClasses() {
        return $this->submit_classes;
    }

    
    public function setSelectClasses($value) {
        $this->select_classes = $value;
        return $this;
    }    
    public function getSelectClasses() {
        return $this->select_classes;
    }

    
    public function setTextareaClasses($value) {
        $this->textarea_classes = $value;
        return $this;
    }    
    public function getTextareaClasses() {
        return $this->textarea_classes;
    }
    
    private function _getFormElementStart() {
        return "<form name='review' class='{$this->getFormClasses()}'>";
    }
    
    private function _getFormElementEnd() {
        $b = rand(1,5); 
        $a = rand(6,9); 
        $actions = array('-' => '-', '+' => '+');
        $s = array_rand($actions);
        
        $tmp = "<div class='clearfix'></div>";
        $tmp .= "<input type='hidden' name='s' value='{$s}'/>";
        $tmp .= "<input type='hidden' name='a' value='{$a}'/>";
        $tmp .= "<input type='hidden' name='b' value='{$b}'/>";
        $tmp .= "<span class='captcha-img'>$a $s $b =</span> <input type='text' name='v' placeholder='?' size='3' max-length='3'/><br/>";
        $tmp .= "<input type='submit' class='{$this->getSubmitClasses()}' value='Submit your review' />";
        $tmp .= "</form>";
        return $tmp;
    }
    
    private function _getRatingElement() {
        $tmp  = "<select name='rating' class='{$this->getSelectClasses()}'>";
            $tmp .= "<option value=''>Select a rating</option>";
            for($i = 1; $i <= 5; $i++) {
                $titles = \Ezy\Ezy::getRatingOptions();
                $title = $titles[$i];
                $tmp .= "<option value='$i'>$title</option>";
            }
        $tmp .= "</select>";
        return $tmp;        
    }
    
    private function _getTextElement() {
        return "<textarea name='text' class='{$this->getTextareaClasses()}' placeholder='Say something about us'></textarea>";
    }
}