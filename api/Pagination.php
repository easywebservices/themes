<?php 
namespace Ezy;

class Pagination {
    
    public $html;
    public $start;
    public $rows;
    public $total;
    public $page;
    
    public function __construct($feed) {
        $this->start = $feed->getStart();
        $this->rows = $feed->getRows();
        $this->total = $feed->getTotal();        
        $this->noOfPages = ceil($this->total / $this->rows);
    }
    
    public function __toString(){
        $lastPageLink =  $firstPageLink = $spaces = $pagClass = "";
        $liStart = "<li>"; $liEnd = "</li>";        
        $this->html .= "<nav><ul id='pagination' class='pagination'>";        
        $this->page = ceil($this->start / $this->rows) + 1;
        
        if ($this->start > 0) {
            $link = \Ezy\Base::getNewUrl(null, array('start' => 0, 'rows' => $this->rows));
            $firstPageLink = $liStart 
                . "<a href='$link' aria-label='Previous'><span aria-hidden='true'>&laquo;</span></a>"
                . $liEnd;
        }
        if ($this->page < $this->noOfPages) {
            $lastPageStart = (($this->noOfPages - 1) * $this->rows);
            $link = \Ezy\Base::getNewUrl(null, array('start' => $lastPageStart, 'rows' => $this->rows));
            $lastPageLink = $liStart 
                . "<a href='$link' aria-label='Next'><span aria-hidden='true'>&raquo;</span></a>"
                . $liEnd;
        }

        if($this->total > $this->rows){
            $this->html .= $firstPageLink;

            if ($this->noOfPages < 11) {
                $this->html .= $this->_showAllPageNumbers();
            }else{
                $this->html .= $this->_showAdjucentPageNumbers();
            }

            $this->html .= $lastPageLink;

        }else{
            $this->html .= "<li class='active'><a href='#pagination'>Page 1 of 1</a></li>";
        }

        $this->html .= "</ul></nav>";
        return $this->html;

    }
    
     private function _showAllPageNumbers(){
        $classActive = $html = "";

        if ($this->noOfPages > 1) {
            for($i=1; $i<=$this->noOfPages; $i++){
                $tmp1 = (($i-1) * $this->rows);
                $iStart = ($tmp1 ? $tmp1 : 0);

                $link = \Ezy\Base::getNewUrl(null, array(
                    'start' => $iStart,
                    'rows' => $this->rows
                ));
                $html .= "<li><a href='$link'>$i</a></li>";
            }

        } else {
            $html .= "<li>Page 1 of 1</li>";
        }

        return $html;
    }

    private function _showAdjucentPageNumbers() {
        $html = $link = "";

        if($this->start >= $this->rows){
            $i = $this->page - 1;
            $j = $this->page;
            for($i; $i<$j; $i++){
                if($i>1){
                    $link = \Ezy\Base::getNewUrl(null, array(
                        'start' => ((($i-1) * $this->rows)),
                        'rows' => $this->rows
                    ));
                    $html .= "<li><a href='$link'>$i</a></li>";
                }
           }
        }

        $html .= "<li class='active'><a href='#'>" . $this->page  . " of " . $this->noOfPages . "</a></li>";

        if($this->start+$this->rows < $this->total){
            $i = $this->page + 1;
            $j = $this->page + 1;
            for($i; $i<=$j; $i++){
                if($i < $this->noOfPages){
                    $link = \Ezy\Base::getNewUrl(null, array(
                        'start' => ((($i-1) * $this->rows)),
                        'rows' => $this->rows
                    ));
                    $html .= "<li><a href='$link'>$i</a></li>";
                }
           }
        }

        return $html;
    }
}

