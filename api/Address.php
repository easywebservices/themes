<?php
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class Address extends Base {
    public $door_no;
    public $house_name;
    public $road;
    public $town;
    public $city;
    public $postcode;
    public $county;
    public $country;
        
    public function __construct($options = null){
        parent::__construct($options);
    }
    
    
    public function getGoogleMap($options = null){
        $params = array_merge( array(
            'height' => 350,
            'width' => 425,
            'zoom' => 14,
            'id' => 'google-map',
        ), $options);
        
    	$addressString = $this->getAddressArray();
        $addressString = urlencode( implode(',', $addressString));
		return '<iframe '
		. 'id="' . $params["id"] . '" '
		. 'width="' . $params["width"] . '" '
		. 'height="' . $params["height"] . '" '
		. 'frameborder="0" scrolling="no" marginheight="0" marginwidth="0" '
		. 'src="https://www.google.co.uk/maps?'
		. 't=m'
		. '&amp;q=' . $addressString
		. '&amp;ie=UTF8'
		. '&amp;hq='
		. '&amp;hnear=' . $addressString
		. '&amp;z=' . $params['zoom']
		. '&amp;output=embed">
		</iframe>';
    }
    
    
    public function getAddressArray(){
    	$array = array();
    	( $this->getDoorNo() ? $array['door_no'] = $this->getDoorNo() : '' );
    	( $this->getHouseName() ? $array['house_name'] = $this->getHouseName() : null);
    	( $this->getRoad() ? $array['road'] = $this->getRoad() : '');
       	( $this->getTown() ? $array['town'] = $this->getTown() : '');
       	( $this->getCity() ? $array['city'] = $this->getCity() : '');
    	( $this->getPostcode() ? $array['postcode'] = $this->getPostcode() : '');
    	( $this->getCounty() ? $array['county'] = $this->getCounty() : '');
    	( $this->getCountry() ? $array['country'] = $this->getCountry() : '');
    	return $array;
    }
    
    
    /*****************************************
    *
    * 	Model_Address Setters and Getters 
    *
    ******************************************/
   
    public function setDoorNo($value){
        $this->door_no = $value;
        return $this;
    }
    public function getDoorNo(){
        return $this->escape($this->door_no);
    }    
    
    public function setHouseName($value){
        $this->house_name = $value;
        return $this;
    }
    public function getHouseName(){
        return $this->escape($this->house_name);
    }    
    
    public function setRoad($value){
        $this->road = $value;
        return $this;
    }
    public function getRoad(){
        return $this->escape($this->road);
    }    
    
    public function setTown($value){
        $this->town = $value;
        return $this;
    }
    public function getTown(){
        return $this->escape($this->town);
    }    
    
    public function setCity($value){
        $this->city = $value;
        return $this;
    }
    public function getCity(){
        return $this->escape($this->city);
    }    
    
    public function setPostcode($value){
        $this->postcode = $value;
        return $this;
    }
    public function getPostcode(){
        return $this->escape($this->postcode);
    }    
    
    public function setCounty($value){
        $this->county = $value;
        return $this;
    }
    public function getCounty(){
        return $this->escape($this->county);
    }     
    
    public function setCountry($value){
        $this->country = $value;
        return $this;
    }
    public function getCountry(){
        return $this->escape($this->country);
    }
}
