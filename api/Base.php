<?php
namespace Ezy;
include_once('Ezy.php');
include_once('EzyException.php');

use \Ezy\Ezy as Ezy;
use \Ezy\EzyException as EzyException;

abstract class Base {  
    const DEFAULT_VIDEO_ID = '';
    const DEFAULT_THEME_ALBUM_DIR = '../../../default-albums';     
    const THEMES_DIR = 'themes/';         
    const THEMES_LAYOUT_FILE_NAME = 'layout.phtml';         
    const THEMES_SCREENSHOT_DIR_NAME = 'screenshots';
    const THEMES_CONF_FILE_NAME = 'configs.php';         
    const DEFAULT_FACEBOOK_PAGE_URL = 'https://www.facebook.com/curry.hunter.online';
    const DEFAULT_GOOGLE_PLUS_PAGE_URL = 'https://plus.google.com/+GoogleBusiness/posts';
    const ALBUM_PHOTO_URL = "/admin/global/photo";    
    const ALBUM_PHOTO_THUMB_URL = "/admin/global/photo-thumbnail";
    const REVIEW_RATING_MAX = 5;
    const DEFAULT_FOOTER_LINK = "http://curryhunter.com";
    const DEFAULT_FOOTER_LINK_TEXT = 'CurryHunter';
    const DEFAULT_POWERED_BY = 'Powered by CurryHunter';
    
    const MENU_CATEGORY_TYPE_CLASSIC = 'classic';
    const MENU_CATEGORY_TYPE_NORMAL = 'normal';
        
    static $allowed_countries = array(
        'FR' => 'France',
        'GB' => 'United Kingdom',
        'US' => 'United States',
    );
       
    const STATUS_QUEUED = '1a31b0793a5a0dfe22957e34fcae87f68e';
    const STATUS_PUBLISHED = '95fded8124e8426212aa8cddba37a2d02ef7';    
    const STATUS_RETIRED = '7cba85fc51d6c9e76a88b6114031870bcc8f';
    const STATUS_REPORTED = 'ff9abac4965f9aa6dd9120a84d4fec4';
  
    const RESTAURANT_LOGO_URL = "/admin/global/logo";    
    const CATEGORY_PHOTO_URL = "/admin/global/menu-category-photo";    
    const CATEGORY_PHOTO_THUMB_URL = "/admin/global/menu-category-thumbnail";
    const ALBUM_PHOTO_MAX = 6;    
    const TEXT_SENDING_LIMIT = 3; // in one session
            
    const DEFAULT_IMAGE_WIDTH = 700;
    const DEFAULT_THUMBNAIL_WIDTH = 120;
    const DEFAULT_THUMBNAIL_HEIGHT = 120;
    const DEFAULT_THUMBNAIL_PATH = '/images/default.png';
        
    const DEFAULT_FILE_NAME = 'global';
    const DEFAULT_LAYOUT_FILE_NAME = 'layout.phtml';
        
    const CONTROLLER_INDEX = '/restaurant/index/';
    const CONTROLLER_REVIEW = '/restaurant/reviews/';
    const CONTROLLER_MENU = '/restaurant/menu/';
    const CONTROLLER_VIDEO = '/restaurant/videos/';
    const CONTROLLER_ALBUM = '/restaurant/albums/';
    const CONTROLLER_OFFER = '/restaurant/offers/';
    const CONTROLLER_RESOURCE = '/restaurant/resource/';
   
    public function __construct(array $options = null) {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }
    
    public function getFunctionNameFromFieldName($name){
        $parts = explode( '_' , $name );        
        @$newParts = array_map('ucfirst',$parts);        
        return implode($newParts);
    }
 
    public function __get($name){
        $method = 'get' . $this->getFunctionNameFromFieldName($name);
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new EzyException('Invalid note property');
        }
        return $this->$method();
    }
 
    public function setOptions(array $options){        
        foreach ($options as $key => $value) {
            $this->$key = $value;
        }
        return $this;
    }    
    
    public static function appendParamsIntoUrl($params = null, $url = null){
        if($url && strpos($url, "?") && $params) {
            $url .= "&" . http_build_query($params);
        } elseif($url && $params) {
            $url .= "?" . http_build_query($params);
        }elseif(!$url && $params){
            $url .= "?" . http_build_query($params);
        }

        return $url;
    }
    
    public static function getNewUrl($url = null, $params = array()){
        if(null === $url){
            $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        }
        
        if(empty($params)){
            return $url;
        }

        $parsed_url = parse_url( $url );
        $newUrl = '';
        
        if(isset($parsed_url['host'])){
            $newUrl .= (isset($parsed_url['scheme']) ? $parsed_url['scheme'] . "://" : "http://");
            $newUrl .= $parsed_url['host'];
            $newUrl .= (isset($parsed_url['path']) ? $parsed_url['path'] : null);
        }else{
            $newUrl .= $url;
        }

        $old_params_array = array();
        if(isset($parsed_url['query'])){
            $old_params = explode('&', $parsed_url['query']);            
            foreach($old_params as $param){
                $param = explode('=', $param);
                $key = $param[0];
                $value = $param[1];
                
                if($key && $value){
                    $old_params_array[ $key ] = $value;
                }
            }
        }
        
        $new_params = array_merge($old_params_array, $params);
        foreach ($params as $key => $value) {
            if (null === $value) {
                unset($new_params[$key]);
            }
        }
        
        $newUrl .= (!empty($params) ? "?" . http_build_query($new_params) : null);
        $newUrl .= (isset($parsed_url['fragment']) ? $parsed_url['fragment'] : null);
        
        return $newUrl;        
    }
    
    public static function escape($string){
        return htmlspecialchars($string);
    }
}
