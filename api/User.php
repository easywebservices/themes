<?php 
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class User extends Base {

    public $mobile;
    public $name;
    public $country_iso;
    public $platform;
    public $platform_id;
    public $mobile_verified;
    public $secondary_contact;
    public $secondary_contact_verified;
    
    public $url;
    public $photo_url;
    public $gender;
    public $birthday;
    public $phone;
    public $access_level;
    
    public $door_no;
    public $house_name;
    public $road;
    public $town;
    public $city;
    public $postcode;
    public $county;
    public $country; 
    
    private $_default_profile_required_scope = array();        
    
    const ACCESS_LEVEL_CUSTOMER = '0d8a55f91a0f848f0db4d9d6467c6f451facea17007b33903'; // Customer
    const ACCESS_LEVEL_GUEST = 'a4ed284c378a4a9531c98f33f0df9da96e2ba48b7a681e8';    // Guest users
    const CURRENT_USER = 'current_user'; // this needs to math the User_Model_User::CURRENT_USER

    
    public function __construct($options = null) {
        parent::__construct($options);
    }
    
    public function isCustomer(){
        return ($this->getAccessLevel() == self::ACCESS_LEVEL_CUSTOMER);
    }
    
    public function isGuest(){
        return ($this->getAccessLevel() == self::ACCESS_LEVEL_GUEST);
    }
    
    public function isLoggedIn(){
        return ($this->getAccessLevel() == self::ACCESS_LEVEL_CUSTOMER);
    }
    
    public function profileComplete($scope = null){
        if(!$scope){
            $scope = $this->_default_profile_required_scope;
        }
        
        $complete = true;
        $vars = get_object_vars($this);
        
        foreach($scope as $v){        
            $complete = (isset($vars[ $v ]));            
            if(! $complete) break;
        }
        
        return $complete;
    }
    
    public function isAuthorised(){
        return true;
    }
    
    public function getAddress(){
        return new User_Model_Address(array(
            'door_no' => $this->getDoorNo(),
            'house_name' => $this->getHouseName(),
            'road' => $this->getRoad(),
            'town' => $this->getTown(),
            'city' => $this->getCity(),
            'postcode' => $this->getPostcode(),
            'county' => $this->getCounty(),
            'country' => $this->getCountry()
        ));
    }
    
    public static function getAccessLevels(){
        // can't change the sequence
        return array(
            1 => self::ACCESS_LEVEL_CUSTOMER,
            0 => self::ACCESS_LEVEL_GUEST,
        );
    }
    
    public static function getAccesslevelKeyByValue( $value ){
        $als = self::getAccessLevels();        
        return array_search($value, $als);
    }
    
    public static function getAccessLevelByKey( $key ){
        $als = self::getAccessLevels();        
        return @$als[$key ];
    }
    
    public function getDisplayName(){
        return $this->getName();
    } 
    
    public function isFacebookUser(){
        return ($this->getPlatform() == 'facebook');
    }

    
    public function setLocale($locale) {        
        $l = Zend_Locale(Zend_Locale::BROWSER);
        $l->setLocale($locale);        
        setcookie(self::COOKIE_USER_LOCALE, $locale, self::COOKIE_USER_EXPIRE_IN);
        return $this;
    }
    
    public function getLocale() {
        $request = new Zend_Controller_Request_Http();
        if ($request->getCookie(self::COOKIE_USER_LOCALE)) {
            echo $request->getCookie(self::COOKIE_USER_LOCALE);
            return $request->getCookie(self::COOKIE_USER_LOCALE);
        }
        
        $locale = new Zend_Locale(Zend_Locale::BROWSER);
        return $locale->toString();
    }
    
    /*
    *
    *       GETTERS & SETTERS
    *
    */

         
    public function getId(){
        return md5( $this->getMobile() ); 
    }   
    
    public function setPlatform($value){
        $this->platform = $value;
        return $this;
    }
    public function getPlatform(){
        return $this->platform; 
    }    
    
    public function setPlatformId($value){
        $this->platform_id = $value;
        return $this;
    }
    public function getPlatformId(){
        return $this->platform_id; 
    }  
    
    public function setCountryIso($value){
        $this->country_iso = $value;
        return $this;
    }
    public function getCountryIso(){
        return $this->country_iso; 
    }
         
    public function setSecondaryContact($value){
        $this->secondary_contact = $value;
        return $this;
    }
    public function getSecondaryContact(){
        return $this->secondary_contact; 
    }  
         
    public function setSecondaryContactVerified($value){
        $this->secondary_contact_verified = $value;
        return $this;
    }
    public function getSecondaryContactVerified(){
        return $this->secondary_contact_verified; 
    }  
         
    public function setMobile($value){
        $this->mobile = $value;
        return $this;
    }
    public function getMobile(){        
        return $this->mobile;
    }

    public function setEmail($value){
        return $this->secondaryContact($value);
    }
    public function getEmail(){
        return $this->getSecondaryContact();
    }     
    
    
    public function setPhone($value){
        $this->phone = $value;
        return $this;
    }
    public function getPhone(){
        return $this->escape($this->phone);
    }   
    
    public function setUrl($value){
        $this->url = $value;
        return $this;
    }
    public function getUrl(){
        return $this->url;
    }   
    
    public function setPhotoUrl($value){
        $this->photo_url = $value;
        return $this;
    }
    public function getPhotoUrl($arg1 = 'large', $arg2 = null){
        if (null === $this->photo_url) {
            if ($this->isFacebookUser()) {
                //supports only -> small, normal, large, square
                $this->photo_url = "http://graph.facebook.com/{$this->getPlatformId()}/picture?type=$arg1";
            }
        }
        
        return $this->photo_url;
    }      
    
    public function setBirthday($value){
        $this->birthday = $value;
        return $this;
    }
    public function getBirthday(){
        return $this->birthday;
    }     
    
    public function setGender($value){
        $this->gender = $value;
        return $this;
    }
    public function getGender(){
        return $this->escape($this->gender);
    }    
         
    public function setMobileVerified($value){
        $this->mobile_verified = $value;
        return $this;
    }
    public function getMobileVerified(){
        return $this->mobile_verified; 
    } 
         
    public function setName($value){
        $this->name = $value;
        return $this;
    }
    public function getName(){
        return $this->name; 
    } 
             
    public function setAccessLevel($value){
        if(strlen($value) == 1){
            $value = Restaurant_Model_Ezy::getAccessLevelByKey($value);
        }
        $this->access_level = $value;
        return $this;
    }
    public function getAccessLevel(){
        return $this->access_level;
    }    
    
    public function setStatus($value){
        if(strlen($value) == 1){
            $value = Restaurant_Model_Ezy::getStatusByKey($value);
        }
        $this->status = $value;
        return $this;
    }
    public function getStatus($key = false){
        return $this->status;
    }
    
    public function setDoorNo($value){
        $this->door_no = $value;
        return $this;
    }
    public function getDoorNo(){
        return $this->escape($this->door_no);
    }    
    
    public function setHouseName($value){
        $this->house_name = $value;
        return $this;
    }
    public function getHouseName(){
        return $this->escape($this->house_name);
    }    
    
    public function setRoad($value){
        $this->road = $value;
        return $this;
    }
    public function getRoad(){
        return $this->escape($this->road);
    }    
    
    public function setTown($value){
        $this->town = $value;
        return $this;
    }
    public function getTown(){
        return $this->escape($this->town);
    }    
    
    public function setCity($value){
        $this->city = $value;
        return $this;
    }
    public function getCity(){
        return $this->escape($this->city);
    }    
    
    public function setPostcode($value){
        $this->postcode = $value;
        return $this;
    }
    public function getPostcode(){
        return $this->escape($this->postcode);
    }    
    
    public function setCounty($value){
        $this->county = $value;
        return $this;
    }
    public function getCounty(){
        return $this->escape($this->county);
    }     
    
    public function setCountry($value){
        $this->country = $value;
        return $this;
    }
    public function getCountry(){
        return $this->escape($this->country);
    }     
    
    public function setUtcOffset($value){
        $this->utc_offset = $value;
        return $this;
    }
    public function getUtcOffset(){
        return $this->escape($this->utc_offset);
    }  
}