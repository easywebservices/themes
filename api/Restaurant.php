<?php
namespace Ezy;

include_once('Base.php');
include_once('Address.php');
include_once('Album.php');
include_once('Feed.php');
include_once('Menu.php');

include_once('MenuItem.php');
include_once('MenuItemClassic.php');
include_once('MenuItemNormal.php');
include_once('Offer.php');
include_once('Page.php');
include_once('Photo.php');
include_once('Review.php');
include_once('Seo.php');
include_once('Social.php');
include_once('Theme.php');
include_once('Time.php');
include_once('User.php');
include_once('Video.php');

use \Ezy\Base as Base;
use \Ezy\Address as Address;
use \Ezy\Album as Album;
use \Ezy\Feed as Feed;
use \Ezy\Menu as Menu;

use \Ezy\MenuItem as MenuItem;
use \Ezy\MenuItemClassis as MenuItemClassis;
use \Ezy\MenuItemNormal as MenuItemNormal;
use \Ezy\Offer as Offer;
use \Ezy\Page as Page;
use \Ezy\Photo as Photo;
use \Ezy\Review as Review;
use \Ezy\Seo as Seo;
use \Ezy\Social as Social;
use \Ezy\Theme as Theme;
use \Ezy\User as User;
use \Ezy\Time as Time;
use \Ezy\Video as Video;

/**
 * A class for handling the restaurant data
 * 
 * 
 * @see Base.php
 * @package Ezy
 * @version 1.0
 *
 * @author Md Abdullah Al Maruf <maruf.sylhet@gmail.com>
 */

class Restaurant extends \Ezy\Base {

    public $id;
    public $default_album_id;

    public $name;
    public $type;
    public $slogan;    
    public $logo_image_id;
    public $description;
    public $package;

    public $no_of_seats;
    public $phone_reservation;
    public $phone_takeaway;
    public $delivery_radius;

    public $door_no;
    public $house_name;
    public $road;
    public $town;
    public $city;
    public $postcode;
    public $county;
    public $country;

    public $social_facebook_page_url;
    public $social_google_plus_page_url;

    public $seo_varification_google;
    public $seo_varification_bing;
    public $seo_ga_account_id;
    public $seo_description;
    public $seo_keywords;

    public $theme_ref;    
    public $theme_color_scheme;    
    
    public $default_image_width;
    public $default_video_id;

    public $website_currency;
    public $website_language;
    public $website_url;
    public $website_availabile;
    public $website_show_spice_level;
    public $website_show_recommend_button;
  
    public $website_footer_link;
    public $website_footer_link_text;
    public $website_powered_by_text;

    public $show_page_offers;
    public $show_page_about;
    public $show_page_reviews;
    public $show_page_videos;
    public $show_page_photos;
    public $show_page_reservation;

    public $accept_cheque;
    public $accept_cash;
    public $accept_card_visa;
    public $accept_card_master;
    public $accept_card_maestro;
    public $accept_card_american_express;

    public $available_eatin;
    public $available_takeaway;
    public $available_delivery;
  
    public $status; // { Enabled, Disabled }
    public $ts;

    public $albums;
    public $offers;
    public $reviews;
    public $times;
    public $customers;
    public $videos;
    public $pages;

    public $menu;
    public $owner;
    public $theme;
    
    private  $_default_album;
    private  $_logo;
    
    public function __construct($options = null){
        parent::__construct($options);
    }
	
	public function getSubUnsubLink() {
		return "<a href='/restaurant/notification/subscribe'>Subscribe to</a>/"
		. "<a href='/restaurant/notification/unsubscribe'>Unsubscribe from</a> our mailing list.";
	}
    
    public function getThemeViewHelpers() { 
        return $this->getTheme()->getHelpers();
    }

    /**
    * Get the theme directory location
    * 
    * @return String
    */
    public function getThemeDirectoryLocation(){
        if($this->getThemeRef()){
            return APPLICATION_PATH . '/' . self::THEMES_DIR .  $this->getThemeRef() . '/';
        }
    }    
    
    /**
    * Get the currency symbol for this restaurant
    * 
    * @return   Character 
    */
    public function getCurrencySymbol(){
        if($this->getWebsiteCurrency()){
            $symbols = Ezy::getCurrencySymbols();
            return ( isset($symbols[$this->getWebsiteCurrency()]) ? $symbols[$this->getWebsiteCurrency()] : null) ;
        }
    }
    
    /**
    * Get the url for the wanted page
    * 
    * @param    String      $controller     The controller name(Restricted to Album, Video, Review, Menu, Index etc)
    * @param    String      $notUsed        Non-used parameter - Used in the admin side
    * @param    String      $params         Parameter to be added to the URL
    * @return   String   
    */
    public function getUrl($controller = 'home', $notUsed = null, $params = array()){
        return $this->appendParamsIntoUrl(array_merge($params, array(
            'page' => $controller,
            'theme' => $this->getTheme()->getId(),
            'data' => (isset($_GET['data']) ? $_GET['data'] : 'data'),
            'color_scheme' => (isset($_GET['color_scheme']) ? $_GET['color_scheme'] : null), // abandoned ?
            'selected_color_scheme' => (isset($_GET['selected_color_scheme']) ? $_GET['selected_color_scheme'] : null),
        )), null);
    }
    
    /**
    * Whether of not the provided video is the restaurant's default video
    * 
    * @param    Video      $video     The video in question
    * @return   Boolean
    */
    public function isDefaultVideo(\Ezy\Video $video){
        return ($this->getDefaultVideoId() == $video->getId());
    }
            
    
    /**
    * Whether this restaurant is published
    * 
    * @return   Boolean
    */
    public function isPublished(){
        return ($this->getStatus() == Ezy::STATUS_PUBLISHED);
    }
    
    /**
    * Get the restaurant owner object
    * 
    * @return   User
    */
    public function getOwner(){
        if(null === $this->owner){
            $this->owner = $this->getRestaurantMapper()->getOwner($this);
        }
        
        return $this->owner;
    }

    /**
    *   Get the restaurant theme
    *
    *   @return Theme
    */
    public function getTheme(){
        if (!$this->theme instanceof \Ezy\Theme) {           
            $themeRef = (isset($_GET['theme']) ? $_GET['theme'] : 'theme1');
            
            $this->theme = new Ezy\Theme();
            $this->theme->setRef($themeRef)->setName($themeRef);
            
            if (!$themeRef || !is_dir($this->theme->themeDirectoryLocation())) {
                throw new Exception("$themeRef is not a valid theme name. Theme directory have to be a valid directory created here");
            }
            
            if (!$this->theme->isValid()) {
                throw new Exception("$themeRef failed validation.");
            }
            
            $options = include_once($theme->themeDirectoryLocation() . self::CONFIG_FILE_NAME);
            $this->theme = new \Ezy\Theme($options);
        }
        
        return $this->theme;
    }
    /**
    * Set the restaurant theme
    *   
    * @param    Theme   $theme
    * @return   Restaurant
    */
    public function setTheme(\Ezy\Theme $theme) {
        $this->theme = $theme;
        return $this;
    }

    /**
    *   Get the restaurant address
    *
    *   @return Address
    */
    public function getAddress(){
        $options = array(
            'door_no' => $this->getDoorNo(),
            'house_name' => $this->getHouseName(),
            'road' => $this->getRoad(),
            'town' => $this->getTown(),
            'city' => $this->getCity(),
            'postcode' => $this->getPostcode(),
            'county' => $this->getCounty(),
            'country' => $this->getCountry(),
        );

        return new Address($options);
    }

    /**
    *	Get Social network details
    *
    *	@return Social
    */
    public function getSocial(){
    	$options = array(
    		'facebook_page_url' => ( $this->getSocialFacebookPageUrl() ?
                $this->getSocialFacebookPageUrl() : self::DEFAULT_FACEBOOK_PAGE_URL ),
    		'google_plus_page_url' => ( $this->getSocialGooglePlusPageUrl() ?
                $this->getSocialGooglePlusPageUrl() : self::DEFAULT_GOOGLE_PLUS_PAGE_URL ),
    	);

    	return new Social($options);
    }

    /**
    *	Get general and default SEO details of the website
    *
    *	@return Seo
    */
    public function getSeo(){
    	$title = $this->getName() . " | " . $this->getSlogan();

        $keywords = $this->getSeoKeywords();
    	if(!$keywords){
            $address = $this->getAddress();
            if($address){
                $keywords =  implode(', ', array(
                    $this->getName(),
                    $address->getRoad(),
                    $address->getTown(),
                    $address->getPostcode()
                ));
            }else{
                $keywords = $this->getName();
            }
    	}

    	$options = array(
    		'google_site_varification_content' => $this->getSeoVarificationGoogle(),
    		'bing_site_varification_content' => $this->getSeoVarificationBing(),
    		'google_analytics_account_id' => $this->getSeoGaAccountId(),
    		'head_keywords' => $keywords,
    		'head_title' => $title,
    		'head_description' => ( $this->getSeoDescription() ? $this->getSeoDescription() : strip_tags($this->getDescription()) )
    	);

    	return new Seo($options);
    }


    /**
    * Get Google map embed code, optionally you can provide with 
    * height, width, zoom level in the params array
    *
    * $param    array   $params('height' => 425, 'width' => 350, 'zoom' => 15)
    * @return HTML
    */
    public function getGoogleMap($params = array(
        'height' => 425,
        'width' => 350,
        'zoom' => 15
    )){
    	$address = $this->getAddress();
    	return $address->getGoogleMap($params);
    }
    
    
    /**
    *  Get the restaurant Menu , Default is to get out menu, forced to get the menu every time getMenu is called   
    *
    *  @return       Menu
    */
    public function getMenu(){
        if (!$this->menu instanceof \Ezy\Menu) {
            $this->menu = new Menu($this->menu);
        }

        return $this->menu;
    }


    /**
    * Get a specific page for the restaurant
    * 
    * @param    String                  The name of the page
    * @return   Page   
    */
    public function getPage($name){
        $pageNames = Ezy::getPageNames();
        if(false === array_search($name, $pageNames)){
            throw new EzyException("Undefined Page '$name'");
        }
        
        $pages = $this->getPages()->getItems();
        return (isset($pages[ $name ]) ? $pages[ $name ] : new \Ezy\Page(array('name' => $name)));
    }
    
    
    public function getPageMenu(){
        return $this->getPage('menu');
    }
    
    public function getPageAbout(){
        return $this->getPage('about');
    }
    
    public function getPageReviews(){
        return $this->getPage('reviews');
    }
    
    public function getPageHome(){
        return $this->getPage('home');
    }
    
    public function getPageOffers(){
        return $this->getPage('offers');
    }
    
    public function getPagePhotos(){
        return $this->getPage('photos');
    }
    
    public function getPageVideos(){
        return $this->getPage('videos');
    }

    /**
    * Get Page list for the restaurant
    *
    * @return Feed
    */
    public function getPages() {
        if (! $this->pages instanceof Feed && is_array($this->pages)) { 
            $this->pages = new Feed($this->pages);
        }
        return $this->pages;
    }

    private function setPages(Feed $value){
        $this->pages = $value;
        return $this;
    }
    
    
    /**
    * Get offer list of the published restaurant
    *
    * @param    Integer     $rows       no of rows to get
    * @param    Integer     $start      start index
    * @return   Feed   
    */
    public function getOffers($rows = 20, $start = 0) {
        if (! $this->offers instanceof Feed && is_array($this->offers)) { 
            $this->offers = new Feed($this->offers);
        }

        return $this->offers;
    }

    private function setOffers(Feed $value){
        $this->offers = $value;
        return $this;
    }


    /**
    * Get the list of recent published reviews
    *
    * @param    Integer     $rows       no of rows to get
    * @param    Integer     $start      start index
    * @return Feed
    */
    public function getReviews($rows = 20, $start = 0){
        if (! $this->reviews instanceof Feed && is_array($this->reviews)) { 
            $this->reviews = new Feed($this->reviews);
        }

        return $this->reviews;
    }
    
    private function setReviews(Feed $value){
        $this->reviews = $value;
        return $this;
    }
    
    /**
    * Get the feed of videos
    * 
    * @param    Integer     $rows       no of rows to get
    * @param    Integer     $start      start index
    * @return Feed
    */
    public function getVideos($rows = 20, $start = 0){
        if (! $this->videos instanceof Feed && is_array($this->videos)) { 
            $this->videos = new Feed($this->videos);
        }
        
        return $this->videos;
    }

    private function setVideos(Feed $value){
        $this->videos = $value;
        return $this;
    }
    
    /**
    * Get the list of Opening time for the restaurant
    *
    * @param    Integer     $rows       no of rows to get
    * @param    Integer     $start      start index
    * @return   Feed
    */
    public function getTimes($rows = 20, $start = 0){
        if (! $this->times instanceof Feed && is_array($this->times)) { 
            $this->times = new Feed($this->times);
        }

        return $this->times;
    }

    private function setTimes(Feed $value){
        $this->times = $value;
        return $this;
    }



    /**
    * Get the list of albums
    *
    * @param    Integer     $rows       no of rows to get
    * @param    Integer     $start      start index
    * @return   Feed
    */
    public function getAlbums($rows = 20, $start = 0){
        if (! $this->albums instanceof Feed && is_array($this->albums)) { 
            $this->albums = new Feed($this->albums);
        }

        return $this->albums;
    } 
    
    private function setAlbums(Feed $value){
        $this->albums = $value;
        return $this;
    }  
    
    
    /**
    * Get the default photo album for the restaurant
    *
    * @return   Album
    */
    public function getDefaultPhotoAlbum(){
        if (! $this->_default_album instanceof Ezy\Album) {
            if ($this->getDefaultAlbumId()) {
                // tricking  - just returning the first album in case of the test data
                $albums = $this->getAlbums();
                foreach($albums->getItems() as $album) {
                    $this->_default_album = $album; 
                    return $this->_default_album;
                }
            } elseif ($this->getTheme()) {
				$type = ($this->getType() ? $this->getType() : 'other');
                $this->_default_album = $this->getTheme()->getAlbum($type);
            } else {
                $mapper = new Restaurant_Model_Mapper_Theme();
				$type = ($this->getType() ? $this->getType() : 'other');
                $this->_default_album = $mapper->getDefaultTheme()->getAlbum($type);
            }
        }
        
        return $this->_default_album;
    }



    /**
    * Get the list of restaurant customers
    *
    * @param    Integer     $rows       no of rows to get
    * @param    Integer     $start      start index
    * @return   Feed
    */
    public function getCustomers($rows = 20, $start = 0){
        if (! $this->customers instanceof Feed && is_array($this->customers)) { 
            $this->customers = new Feed($this->customers);
        }

        return $this->customers;
    }

    private function setCustomers(Feed $value){
        $this->customers = $value;
        return $this;
    }


    /**
    *   Getters for basic restaurant info
    */
    private function setId($value){
        $this->id = $value;
        return $this;
    }
    
    /**
    * Get the unique identifier/id/# for the restaurant
    *
    * @return   String      an unique md5 hash for identifying the restaurant
    */
    public function getId(){
        return $this->id;
    }
    /**
    * Get the unique identifier/id/# for the restaurant
    *
    * @return   String      an unique md5 hash for identifying the restaurant
    */
    public function getRef(){
        return $this->getId();
    }

    /**
    * Get the name of the restaurant
    *
    * @return   String
    */
    public function getName(){
        return $this->escape($this->name);
    }
    private function setName($value){
        $this->name = $value;
        return $this;
    }  

    /**
    * Get the type of the restaurant
    *
    * @return   String
    */
    public function getType(){
        return $this->escape($this->type);
    }
    private function setType($value){
        $this->type = $value;
        return $this;
    }        
    
    
    /**
    * Get the slogan of the restaurant
    *
    * @return   String
    */
    public function getSlogan(){
        return $this->escape($this->slogan);
    }
    private function setSlogan($value){
        $this->slogan = $value;
        return $this;
    }

    
    /**
    * Get the logo image HTML 
    *
    * @param    String      $class      Specify the bootstrap class for the logo image
    * @param    String      $width      Specify the width of the image in px ie. 120px
    * @return   String      HTML for the image
    */
    public function getLogo($class='img-thumbnail', $width = '120px') {    
        if (null === $this->_logo && $this->getLogoImageId()) {
            $this->_logo = "<img width='$width' class='$class' src='{$this->getLogoImageId()}' alt='logo' title='{$this->getName()}'/>";
        }
        
        return $this->_logo;
    }    
    public function getLogoImageId(){
        return $this->logo_image_id;
    }
    public function setLogoImageId($value){
        $this->logo_image_id = $value;
        return $this;
    }
    
    /**
    * Get the description of the restaurant
    *
    * @return   String
    */
    public function getDescription(){
        return $this->description;
    }
    private function setDescription($value){
        $this->description = $value;
        return $this;
    }

    
    /**
    * Get the description of the restaurant
    *
    * @return   String
    */
    public function getPackage(){
        return $this->escape($this->package);
    }
    private function setPackage($value){
        $this->package = $value;
        return $this;
    }

    /**
    * Get the description of the restaurant
    *
    * @return   String
    */
    public function getPhoneReservation(){
        return $this->escape($this->phone_reservation);
    }
    private function setPhoneReservation($value){
        $this->phone_reservation = $value;
        return $this;
    }

    /**
    * Get the description of the restaurant
    *
    * @return   String
    */
    public function getPhoneTakeaway(){
        return $this->escape($this->phone_takeaway);
    }
    private function setPhoneTakeaway($value){
        $this->phone_takeaway = $value;
        return $this;
    }

    /**
    * Get the the set delivery radios of the restaurant
    *
    * @return       String
    */
    public function getDeliveryRadius(){
        return $this->escape($this->delivery_radius);
    }
    private function setDeliveryRadius($value){
        $this->delivery_radius = $value;
        return $this;
    }

     /**
    * Get the the delivery radios distance type
    *
    * @return       Boolean { 0 => Miles, 1 => Kilometres }
    */
    public function setDeliveryRadiusType($value){
        $this->delivery_radius_type = $value;
        return $this;
    }
    public function getDeliveryRadiusType(){
        return $this->escape($this->delivery_radius_type);
    }

    
    /**
    * Get the restaurant's door no
    *
    * @return       String
    */
    public function getDoorNo(){
        return $this->escape($this->door_no);
    }
    private function setDoorNo($value){
        $this->door_no = $value;
        return $this;
    }

    /**
    * Get the restaurant's house name
    *
    * @return       String
    */
    public function getHouseName(){
        return $this->escape($this->house_name);
    }
    private function setHouseName($value){
        $this->house_name = $value;
        return $this;
    }
    

    /**
    * Get the restaurant's road name
    *
    * @return       String
    */
    public function getRoad(){
        return $this->escape($this->road);
    }
    private function setRoad($value){
        $this->road = $value;
        return $this;
    }

    
    /**
    * Get the restaurant's town name
    *
    * @return       String
    */
    public function getTown(){
        return $this->escape($this->town);
    }
    private function setTown($value){
        $this->town = $value;
        return $this;
    }

    
    /**
    * Get the restaurant's city name
    *
    * @return       String
    */
    public function getCity(){
        return $this->escape($this->city);
    }
    private function setCity($value){
        $this->city = $value;
        return $this;
    }

    /**
    * Get the restaurant's post code
    *
    * @return       String
    */
    public function getPostcode(){
        return $this->escape($this->postcode);
    }
    private function setPostcode($value){
        $this->postcode = $value;
        return $this;
    }

    /**
    * Get the restaurant's county/state name
    *
    * @return       String
    */
    public function getCounty(){
        return $this->escape($this->county);
    }
    private function setCounty($value){
        $this->county = $value;
        return $this;
    }

    /**
    * Get the restaurant's country name
    *
    * @return       String
    */
    public function getCountry(){
        return $this->escape($this->country);
    }
    private function setCountry($value){
        $this->country = $value;
        return $this;
    }


    
    /************************************
    * 
    *	States and Times
    *
    *************************************/

    /**
    * Get the restaurant's publication status
    *
    * @return       String
    */
    public function getStatus(){
        return $this->status;
    }
    private function setStatus($value){
        if(strlen($value) == 1){
            $value = Ezy::getStatusByKey($value);
        }
        $this->status = $value;
        return $this;
    }

    /**
    * Get the restaurant's publication status
    * 
    * @param        String      $format     php date() formatting string
    * @return       String
    */
    public function getTs($format = null) {
		if(null!==$format){
            $dtObj = new DateTime($this->ts);
            return $dtObj->format($format);
        }
        return $this->ts;
	}
    private function setTs($dateTime) {
		$this->ts = $dateTime;
		return $this;
	}


    /************************************
    *
    *   Settings
    *
    *************************************/
    /**
    * Whether Eatin is available or not
    *
    * @return Boolean
    */
    public function getAvailableEatin(){
        return $this->available_eatin;
    }
    private function setAvailableEatin($value){
        $this->available_eatin = $value;
        return $this;
    }
    
    /**
    * No of Eatin seats, applied only if getAvailableEatin() is true
    *
    * @return Boolean
    */
    public function getNoOfSeats(){
        if ($this->getAvailableEatin()) {
            return $this->escape($this->no_of_seats);
        }
    }
    private function setNoOfSeats($value){
        $this->no_of_seats = $value;
        return $this;
    }

    /**
    * Whether Takeaway is available or not
    *
    * @return Boolean
    */
    public function getAvailableTakeaway(){
        return $this->available_takeaway;
    }
    private function setAvailableTakeaway($value){
        $this->available_takeaway = $value;
        return $this;
    }

    /**
    * Whether Food delivery is available or not, applied only if getAvailableTakeaway() is true
    *
    * @return Boolean
    */
    public function getAvailableDelivery(){
        if ($this->getAvailableTakeaway()){ 
            return $this->available_delivery;
        }
    }
    private function setAvailableDelivery($value){
        $this->available_delivery = $value;
        return $this;
    }
 
    /**
    * Powered by text, used by developers
    *
    * @return string
    */
    public function getWebsitePoweredByText(){
        if (null === $this->website_powered_by_text) {
            $this->website_powered_by_text = self::DEFAULT_POWERED_BY;
        }
        
        return $this->website_powered_by_text;
    }
    private function setWebsitePoweredByText($value){
        $this->website_powered_by_text = $value;
        return $this;
    }
    
    /**
    * Footer link, used by developers
    *
    * @return URL
    */
    public function getWebsiteFooterLink(){
        if (null === $this->website_footer_link) {
            $this->website_footer_link = self::DEFAULT_FOOTER_LINK;
        }
        
        return $this->website_footer_link;
    }
    private function setWebsiteFooterLink($value){
        $this->website_footer_link = $value;
        return $this;
    }
    
    /**
    * Footer link text, used by developers
    *
    * @return string
    */
    public function getWebsiteFooterLinkText(){
        if (null === $this->website_footer_link_text) {
            $this->website_footer_link_text = self::DEFAULT_FOOTER_LINK_TEXT;
        }
        
        return $this->website_footer_link_text;
    }
    private function setWebsiteFooterLinkText($value){
        $this->website_footer_link_text = $value;
        return $this;
    }
    
    /************************************
    *
    *   Pages to show/not to show 
    *
    *************************************/
    
    /**
    * Whether to show or not Home Page
    * Its always true
    *
    * @return Boolean
    */
    public function getShowPageHome(){
        return true;
    }
    
    /**
    * Whether to show or not Menu Page
    * Its always true
    *
    * @return Boolean
    */
    public function getShowPageMenu(){
        return true;
    }
    
    
    /**
    * Whether to show or not Reviews Page
    * Depending on the settings and package selected
    *
    * @return Boolean
    */
    public function getShowPageReviews(){
        return $this->show_page_reviews;
    }
    private function setShowPageReviews($value){
        $this->show_page_reviews = (bool)$value;
        return $this;
    }
  
    /**
    * Whether to show or not Offers Page
    * Depending on the settings and package selected
    *
    * @return Boolean
    */
    public function getShowPageOffers(){
        return $this->show_page_offers;
    }
    private function setShowPageOffers($value){
        $this->show_page_offers = (bool)$value;
        return $this;
    }
    
  
    /**
    * Whether to show or not About Page
    * Depending on the settings and package selected
    *
    * @return Boolean
    */
    public function getShowPageAbout(){
        return $this->show_page_about;
    }
    private function setShowPageAbout($value){
        $this->show_page_about = (bool)$value;
        return $this;
    }
  
    /**
    * Whether to show or not Videos Page
    * Depending on the settings and package selected
    *
    * @return Boolean
    */
    public function getShowPageVideos(){
        return $this->show_page_videos;
    }
    private function setShowPageVideos($value){
        $this->show_page_videos = (bool)$value;
        return $this;
    }
  
    
    /**
    * Whether to show or not Photos Page
    * Depending on the settings and package selected
    *
    * @return Boolean
    */
    public function getShowPagePhotos(){
        return $this->show_page_photos;
    }
    private function setShowPagePhotos($value){
        $this->show_page_photos = (bool)$value;
        return $this;
    }
  
    
    /**
    * Whether to show or not Reservation Page
    * Depending on the settings and package selected
    *
    * @return Boolean
    */
    public function getShowPageReservation(){
        return $this->show_page_reservation;
    }
    private function setShowPageReservation($value){
        $this->show_page_reservation = (bool)$value;
        return $this;
    }
  
    
    /**
    * Whether to show recommend button
    *
    * @return Boolean
    */
    public function getWebsiteShowRecommendButton(){
        return $this->website_show_recommend_button;
    }
    private function setWebsiteShowRecommendButton($value){
        $this->website_show_recommend_button = (bool)$value;
        return $this;
    }
    

    /************************************
    *
    *   Facebook?Google page settings
    *
    ************************************/

    /**
    * Get the Facebook page URL 
    *
    * @return       String      The URL of the Facebook page
    */
    public function getSocialFacebookPageUrl(){
        return $this->social_facebook_page_url;
    }
    private function setSocialFacebookPageUrl($value){
        $this->social_facebook_page_url = $value;
        return $this;
    }

    /**
    * Get the Facebook page URL 
    *
    * @return       String      The URL of the Facebook page
    */
    public function getSocialGooglePlusPageUrl(){
        return $this->social_google_plus_page_url;
    }
    private function setSocialGooglePlusPageUrl($value){
        $this->social_google_plus_page_url = $value;
        return $this;
    }
    
    
    /************************************
    *
    *   SEO settings
    *
    ************************************/
    
    /**
    * Get the google varification code String
    *
    * @return       String 
    */    
    public function getSeoVarificationGoogle(){
        return $this->escape($this->seo_varification_google);
    }
    private function setSeoVarificationGoogle($value){
        $this->seo_varification_google = $value;
        return $this;
    }

    /**
    * Get the Bing verification String
    *
    * @return       String 
    */
    public function getSeoVarificationBing(){
        return $this->escape($this->seo_varification_bing);
    }
    private function setSeoVarificationBing($value){
        $this->seo_varification_bing = $value;
        return $this;
    }    

    /**
    * Get the google analytics ID like this UA-XXXXXXXX
    *
    * @return       String     
    */
    public function getSeoGaAccountId(){
        return $this->escape($this->seo_ga_account_id);
    }
    private function setSeoGaAccountId($value){
        $this->seo_ga_account_id = $value;
        return $this;
    }

    /**
    * Get the general SEO description for the whole site
    *
    * @return       String     
    */
    public function getSeoDescription(){
        return $this->escape($this->seo_description);
    }
    private function setSeoDescription($value){
        $this->seo_description = $value;
        return $this;
    }    

    /**
    * Get the general SEO keywords for the whole site
    *
    * @return       String     
    */
    public function getSeoKeywords(){
        return $this->escape($this->seo_keywords);
    }
    private function setSeoKeywords($value){
        $this->seo_keywords = $value;
        return $this;
    }

    /************************************
    *
    *   Theme settings for this restaurant
    *
    ************************************/
    
    /**
    * Get the theme # for the restaurants selected theme
    *
    * @return       String     
    */
    public function getThemeId(){
        return $this->getThemeRef();
    }

    /**
    * Get the theme # for the restaurants selected theme
    *
    * @return       String     
    */
    public function getThemeRef(){
        return $this->theme_ref;
    }
    private function setThemeRef($value){
        $this->theme_ref = $value;
        return $this;
    }

    /**
    * Get the chosen color scheme name
    *
    * @return       String     
    */
    public function getThemeColorScheme(){
        // API tricks
        if (isset($_GET['color_scheme']) && null != $_GET['color_scheme']) {        
            $this->theme_color_scheme = $_GET['color_scheme'];
        }
        
        return $this->escape($this->theme_color_scheme);
    }
    /**
    * Get the chosen color scheme name
    *
    * @return       String     
    */
    public function getSelectedColorScheme(){
        return $this->getThemeColorScheme();
    }
    private function setThemeColorScheme($value){
        $this->theme_color_scheme = $value;
        return $this;
    }
    public function getColorSchemeCssFile(){
		if ($this->getSelectedColorScheme()) {
			return $this->getTheme()->getColorSchemeCssFile($this->getSelectedColorScheme());
		}
    }
    
    
    /************************************
    *
    *   Defaults
    *
    ************************************/        
    /**
    * Get the default video #
    *
    * @return       String     
    */
    public function getDefaultAlbumId(){
        return $this->default_album_id;
    }
    private function setDefaultAlbumId($value){
        $this->default_album_id = $value;
        return $this;
    }
    
    /**
    * Get the default video #
    *
    * @return       String     
    */
    public function getDefaultVideoId(){
        if(null === $this->default_video_id){
            $this->default_video_id = self::DEFAULT_VIDEO_ID;
        }
        return $this->default_video_id;
    }
    private function setDefaultVideoId($value){
        $this->default_video_id = $value;
        return $this;
    }
    
    /**
    * Get the default image width for this restaurant
    *
    * @return       String     
    */
    public function getDefaultImageWidth(){
        if(null === $this->default_image_width){
            $this->default_image_width = self::DEFAULT_IMAGE_WIDTH;
        }
        return $this->escape($this->default_image_width);
    }
    private function setDefaultImageWidth($value){
        $this->default_image_width = $value;
        return $this;
    }

    /**
    * Get the default thumbnail height
    *
    * @return       String     
    */
    public function getDefaultThumbnailHeight(){
        return self::DEFAULT_THUMBNAIL_HEIGHT;
    }
    
    /**
    * Get the default thumbnail width for this restaurant
    *
    * @return       String     
    */
    public function getDefaultThumbnailWidth(){
        return self::DEFAULT_THUMBNAIL_WIDTH;
    }

    
    /************************************
    *
    *   Website settings
    *
    ************************************/        
    /**
    * Get the website currency for this restaurant
    *
    * @return       String     
    */
    public function getWebsiteCurrency(){
        return $this->website_currency;
    }
    private function setWebsiteCurrency($value){
        $this->website_currency = $value;
        return $this;
    }

    /**
    * Get the website currency for this restaurant
    *
    * @return       String     
    */
    public function getWebsiteLanguage(){
        return $this->website_language;
    }
    private function setWebsiteLanguage($value){
        $this->website_language = $value;
        return $this;
    }

    /**
    * Get the website URL for this restaurant
    *
    * @return       String     
    */
    public function getWebsiteUrl(){
        return $this->escape($this->website_url);
    }
    private function setWebsiteUrl($value){
        $this->website_url = $value;
        return $this;
    }
    
    /**
    * Get whether the website should be available to general public
    *
    * @return       Boolean     
    */
    public function getWebsiteAvailable(){
        return $this->website_availabile;
    }
    private function setWebsiteAvailable($value){
        $this->website_availabile = $value;
        return $this;
    }

    /**
    * Get whether show the spice level in the website menu
    *
    * @return       Boolean     
    */
    public function getWebsiteShowSpiceLevel(){
        return $this->website_show_spice_level;
    }
    private function setWebsiteShowSpiceLevel($value){
        $this->website_show_spice_level = $value;
        return $this;
    }
    

    /********************************
    *
    *   Accepted payment methods
    *
    *********************************/
    
    /**
    * Get whether to accept cheque as payment method or not
    *
    * @return       Boolean     
    */
    public function getAcceptCheque(){
        return $this->accept_cheque;
    }
    private function setAcceptCheque($value){
        $this->accept_cheque = $value;
        return $this;
    }
    
    
    /**
    * Get whether to accept cash as payment method or not
    *
    * @return       Boolean     
    */
    public function getAcceptCash(){
        return $this->accept_cash;
    }
    private function setAcceptCash($value){
        $this->accept_cash = $value;
        return $this;
    }
    

    /**
    * Get whether to accept VISA card as payment method or not
    *
    * @return       Boolean     
    */
    public function getAcceptCardVisa(){
        return $this->accept_card_visa;
    }
    private function setAcceptCardVisa($value){
        $this->accept_card_visa = $value;
        return $this;
    }

    /**
    * Get whether to accept Master Card as payment method or not
    *
    * @return       Boolean     
    */
    public function getAcceptCardMaster(){
        return $this->accept_card_master;
    }
    private function setAcceptCardMaster($value){
        $this->accept_card_master = $value;
        return $this;
    }
    
    /**
    * Get whether to accept Maestro as payment method or not
    *
    * @return       Boolean     
    */
    public function getAcceptCardMaestro(){
        return $this->accept_card_maestro;
    }
    private function setAcceptCardMaestro($value){
        $this->accept_card_maestro = $value;
        return $this;
    }
    
   
    /**
    * Get whether to accept American Express as payment method or not
    *
    * @return       Boolean
    */
    public function getAcceptCardAmericanExpress(){
        return $this->accept_card_american_express;
    }
    private function setAcceptCardAmericanExpress($value){
        $this->accept_card_american_express = $value;
        return $this;
    }
        
}
