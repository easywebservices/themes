<?php
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class Social extends Base{
    public $google_plus_page_url;
    public $facebook_page_url;
    
    public function __construct($options){
        parent::__construct($options);
    }
    
    public function getFacebookLikeBox($options = array()){
        $params = array_merge( array(
            'height' => 450,
            'width'=> 350,
            'adapt-container-width' => 'true',  // string
            'hide-cover' => 'false',  // string
            'small-header' => 'true',  // string
            'show-facepile' => 'true',  // string
            'show-posts' => 'true',  // string
        ), $options);
        
        return '<div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=361157410714367";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, \'script\', \'facebook-jssdk\'));</script>
        
        <div class="fb-page" data-href="' . $this->getfacebookPageUrl() 
        . '" data-width="' . $params['width']
        . '" data-height="' . $params['height']
        . '" data-small-header="' . $params['small-header']
        . '" data-adapt-container-width="' . $params['adapt-container-width']
        . '" data-hide-cover="' . $params['hide-cover']
        . '" data-show-facepile="' . $params['show-facepile']
        . '" data-show-posts="' . $params['show-posts']
        . '"></div>';
    }
    
    public function getGooglePlusPageBadge($options = array()){
        $params = array_merge( array(
            'layout' => 'landscape',
        ), $options);        
        
        // Portrait layout   180-450 pixels
        // Landscape layout 273-450 pixels        
        
        return '<script type="text/javascript" src="https://apis.google.com/js/plusone.js"></script>
            <g:page class="g-page" data-layout="' . $params["layout"] . '" href="'. $this->getGooglePlusPageUrl() . '"></g:page>';
    }
    
    
    public function setGooglePlusPageUrl($value){
        $this->google_plus_page_url = $value;
        return $this;
    }
    
    public function getGooglePlusPageUrl(){
        if(null === $this->google_plus_page_url){
            $this->google_plus_page_url = Ezy::DEFAULT_GOOGLE_PLUS_PAGE_URL;
        }
        return $this->escape($this->google_plus_page_url);
    }
    
    
    public function setFacebookPageUrl($value){
        $this->facebook_page_url = $value;
        return $this;
    }
    
    public function getFacebookPageUrl(){
        if(null === $this->facebook_page_url){
            $this->facebook_page_url = Ezy::DEFAULT_FACEBOOK_PAGE_URL;
        }
        return $this->escape($this->facebook_page_url);
    }
}
