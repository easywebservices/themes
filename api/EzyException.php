<?php
namespace Ezy;

class EzyException extends \Exception {
    public $message = "";
    public function __construct($message) {
        $this->message = $message;        
    }
    
    public function __toString(){
        return $this->message;
    }
}