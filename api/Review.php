<?php
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class Review extends Base {

    public $user_id;
    public $rating;
    public $text;
    public $status;
    public $ts;
    
    public $customer;
    
    public function __construct($options = null){
        parent::__construct($options);
    }
    
    public function isPublished(){
        return ($this->getStatus() == Ezy::STATUS_PUBLISHED);
    }
    
    /***************************************
    *
    *   SETTERS AND GETTERS 
    *
    *
    *****************************************/
    
    public function setRating($value){
        $this->rating = (int) $value;
        return $this;
    }
    public function getRating(){
        return $this->rating;
    }
    
    public function setText($value){
        $this->text = $value;
        return $this;
    }
    public function getText(){
        return $this->escape($this->text);
    }   
    
    public function setStatus($value){
        if(strlen($value) == 1){
            $value = Ezy::getStatusByKey($value);
        }
        $this->status = $value;
        return $this;
    }
    public function getStatus(){
        return $this->status;
    }     
    
    public function setCustomer(\Ezy\User $value){
        $this->customer = $value;
        return $this;
    }
    public function getCustomer(){
        if (is_array($this->customer)) {
            $this->customer = new \Ezy\User($this->customer);
        }
        
        return $this->customer;
    }  
    
    public function setTs($dateTime) {
		$this->ts = $dateTime;
		return $this;
	}
    public function getTs($format = null) {
        if(null!==$format){
            $dtObj = new \DateTime($this->ts);
            return $dtObj->format($format);
        }
        return $this->ts;
	}
    public function getSubmittedOn($format = null){
        return $this->getTs($format);
    }
    
}
