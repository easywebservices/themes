<?php
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class Album extends Base {

    public $id;
    public $title;
    public $description;
    public $default_pic_id;  
    public $thumbnail_url;  
    public $photos;
     
    public function __construct($options = null){
        parent::__construct($options);
    } 
    
    public function setPhotos($value){
        $this->photos = $value;
        return $this;
    }    
    public function getPhotos() {
        if (!$this->photos instanceof \Ezy\Feed) {
            $pics = $this->photos;
            $this->photos = new \Ezy\Feed();
            
            if (is_array($pics)) {
                $this->photos = new \Ezy\Feed($pics);
            }
        }
        
        return $this->photos;
    }
    
    public function setThumbnailUrl($value){
        $this->thumbnail_url = $value;
        return $this;
    }
    public function getThumbnailUrl(){
        return $this->thumbnail_url;
    }

    
    public function setId($value){
        $this->id = $value;
        return $this;
    }
    public function getId(){
        return $this->id;
    }    
    
    
    public function setTitle($value){
        $this->title = $value;
        return $this;
    }
    public function getTitle(){
        return $this->escape($this->title);
    }
    
    
    public function setDescription($value){
        $this->description = $value;
        return $this;
    }
    public function getDescription(){
        return $this->escape($this->description);
    }
    
    
    public function setDefaultPicId($value){
        $this->default_pic_id = $value;
        return $this;
    }
    public function getDefaultPicId(){
        return $this->default_pic_id;
    } 
}
