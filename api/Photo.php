<?php
namespace Ezy;
include_once('Base.php');
use \Ezy\Base as Base;

class Photo extends Base {

    public $image_url;
    public $thumbnail_url;
    public $caption;
    
    public function __construct($options = null){
        parent::__construct($options);
    }   
   
    public function setImageUrl($value) {
        $this->image_url = $value;
        return $this;
    }
    public function getImageUrl() {
        return $this->image_url ;
    }
    
   
    public function setThumbnailUrl($value) {
        $this->thumbnail_url = $value;
        return $this;
    }
    public function getThumbnailUrl() {
	   if (null === $this->thumbnail_url) {
		 $this->thumbnail_url = $this->getImageUrl();
	 }  
	return $this->thumbnail_url;
	}
    
    
    public function setCaption($value){
        $this->caption = $value;
        return $this;
    }
    public function getCaption(){
        if(null === $this->caption){
            $this->caption = "Default caption";
        }
        return $this->escape($this->caption);
    }
}